﻿--José Alejandro Libreros Montaño 1427406
--Ivan Miguel Viveros 14273415
--David Andres Ramiez 1430933
--Universidad del Valle
--Escuela de Ingenieria de Sisitemas y Computacion

--DROP TABLES
DROP TABLE IF EXISTS empleados CASCADE;
DROP TABLE IF EXISTS estaciones CASCADE;
DROP TABLE IF EXISTS solicitudPQRS CASCADE;
DROP TABLE IF EXISTS accionesPQRS CASCADE;
DROP TABLE IF EXISTS pasajeros CASCADE;
DROP TABLE IF EXISTS buses CASCADE;
DROP TABLE IF EXISTS tarjetas CASCADE;
DROP TABLE IF EXISTS rutas CASCADE;
DROP TABLE IF EXISTS atender CASCADE;
DROP TABLE IF EXISTS registrar_pasaje CASCADE;
DROP TABLE IF EXISTS turnos CASCADE;
DROP TABLE IF EXISTS recargas CASCADE;
--DROP SEQUENCES
DROP SEQUENCE IF EXISTS empleados_seq CASCADE;
DROP SEQUENCE IF EXISTS estaciones_seq CASCADE; 
DROP SEQUENCE IF EXISTS pasajeros_seq CASCADE; 
DROP SEQUENCE IF EXISTS bus_seq CASCADE; 
DROP SEQUENCE IF EXISTS tarjetas_seq CASCADE; 
DROP SEQUENCE IF EXISTS rutas_seq CASCADE; 
DROP SEQUENCE IF EXISTS solicitudes_seq CASCADE; 
DROP SEQUENCE IF EXISTS recargas_seq CASCADE; 
--CREATE SEQUENCES
CREATE SEQUENCE empleados_seq;
CREATE SEQUENCE estaciones_seq START 0 MINVALUE 0;
CREATE SEQUENCE pasajeros_seq;
CREATE SEQUENCE bus_seq;
CREATE SEQUENCE tarjetas_seq;
CREATE SEQUENCE rutas_seq;
CREATE SEQUENCE solicitudes_seq;
CREATE SEQUENCE recargas_seq;

--CREATE TABLES
CREATE TABLE empleados (
  id_empleado VARCHAR(20) DEFAULT nextval('empleados_seq' :: REGCLASS) NOT NULL,
  cedula_empleado VARCHAR(20) UNIQUE NOT NULL,
  nombre_empleado VARCHAR(100) NOT NULL,
  telefono_empleado VARCHAR(10) NOT NULL,
  direccion_empleado VARCHAR(100) NOT NULL,
  email_empleado VARCHAR(100) NOT NULL,
  cargo_empleado VARCHAR(100) NOT NULL,
  salario_empleado DECIMAL NOT NULL,
  id_estacion   VARCHAR(20) NOT NULL,
  estado_empleado  VARCHAR(20) NOT NULL,
  user_empleado  VARCHAR(100) UNIQUE NOT NULL,
  pass_empleado  VARCHAR(255) NOT NULL,
  CONSTRAINT empleado_pk PRIMARY KEY (id_empleado) 
);

CREATE TABLE estaciones (
  id_estacion  VARCHAR(20) DEFAULT nextval('estaciones_seq' :: REGCLASS) NOT NULL,
  nombre_estacion VARCHAR(100) NOT NULL UNIQUE,  
  ubicacion_estacion VARCHAR(100) NOT NULL,
  id_director VARCHAR(20) UNIQUE,
  estado_estacion VARCHAR(20) NOT NULL,
  CONSTRAINT estacion_pk PRIMARY KEY (id_estacion) 
 
);

CREATE TABLE tarjetas(

    pin_tarjeta VARCHAR(20) DEFAULT nextval('tarjetas_seq' :: REGCLASS)  NOT NULL,
    saldo_tarjeta INT NOT NULL,
    estado_tarjeta VARCHAR(20) NOT NULL,
    estacion_id VARCHAR(20) NOT NULL,
	fecha_venta TIMESTAMP NOT NULL,
    CONSTRAINT tarjeta_pk PRIMARY KEY (pin_tarjeta),
    CONSTRAINT solicitud_estacion_fk FOREIGN KEY (estacion_id) REFERENCES estaciones (id_estacion)ON UPDATE CASCADE ON DELETE NO ACTION 
);

CREATE TABLE pasajeros(   
    id_pasajero VARCHAR(20) DEFAULT nextval('pasajeros_seq' :: REGCLASS)  NOT NULL,
    cedula_pasajero VARCHAR(20) NOT NULL UNIQUE,
    nombre_pasajero VARCHAR(100) NOT NULL,
    telefono_pasajero VARCHAR(10) NOT NULL,
    pin_tarjeta VARCHAR(20) NOT NULL,
    CONSTRAINT pasajero_pkey PRIMARY KEY (id_pasajero) ,
    CONSTRAINT pin_tarjeta_fk FOREIGN KEY (pin_tarjeta) REFERENCES tarjetas (pin_tarjeta) ON UPDATE CASCADE ON DELETE NO ACTION
);

CREATE TABLE rutas(
    id_ruta  VARCHAR(20) DEFAULT nextval('rutas_seq' :: REGCLASS)  NOT NULL,
    nombre_ruta VARCHAR(4) NOT NULL  UNIQUE,
    descripcion_recorrido VARCHAR(2000) NOT NULL,
    CONSTRAINT ruta_pkey PRIMARY KEY (id_ruta) 
);

CREATE TABLE solicitudPQRS (
  id_solicitud VARCHAR(20) DEFAULT nextval('solicitudes_seq' :: REGCLASS)  NOT NULL,
  motivo_solicitud VARCHAR(2000) NOT NULL,
  descripcion_solicitud VARCHAR(2000) NOT NULL,
  fecha_solicitud TIMESTAMP NOT NULL,
  estado_solicitud VARCHAR(20) NOT NULL, 
  pasajero_id VARCHAR(20) NOT NULL,
  estacion_id VARCHAR(20) NOT NULL,
  id_empleado VARCHAR(20) NOT NULL DEFAULT '1',
  CONSTRAINT solicitud_pkey PRIMARY KEY (id_solicitud) ,
  CONSTRAINT solicitud_pasajero_fk FOREIGN KEY (pasajero_id) REFERENCES pasajeros (id_pasajero) ON UPDATE CASCADE ON DELETE NO ACTION,
  CONSTRAINT solicitud_estacion_fk FOREIGN KEY (estacion_id) REFERENCES estaciones (id_estacion)ON UPDATE CASCADE ON DELETE NO ACTION
);


CREATE TABLE accionesPQRS (
	id_solicitud  VARCHAR(20),
	accion_solicitud VARCHAR(2000),
	CONSTRAINT acciones_pkey PRIMARY KEY (id_solicitud,accion_solicitud) ,
	CONSTRAINT id_solicitud_fk FOREIGN KEY (id_solicitud) REFERENCES solicitudPQRS (id_solicitud) ON UPDATE CASCADE ON DELETE NO ACTION
);



CREATE TABLE atender(
    id_ruta VARCHAR(20) NOT NULL,
    id_estacion VARCHAR(20) NOT NULL,
    CONSTRAINT atender_pkey PRIMARY KEY (id_ruta,id_estacion) ,
    CONSTRAINT ruta_fk FOREIGN KEY (id_ruta) REFERENCES rutas (id_ruta) ON UPDATE CASCADE ON DELETE NO ACTION,
    CONSTRAINT estacion_fk FOREIGN KEY (id_estacion) REFERENCES estaciones (id_estacion) ON UPDATE CASCADE ON DELETE NO ACTION
);

CREATE TABLE registrar_pasaje(
    id_ruta VARCHAR(20) NOT NULL,
    id_pasajero VARCHAR(20) NOT NULL,
    fecha_hora TIMESTAMP NOT NULL,
    CONSTRAINT registrar_pasaje_pkey PRIMARY KEY (id_ruta,id_pasajero,fecha_hora) ,
    CONSTRAINT ruta_fk FOREIGN KEY (id_ruta) REFERENCES rutas (id_ruta) ON UPDATE CASCADE ON DELETE NO ACTION,
    CONSTRAINT id_pasajero_fk FOREIGN KEY (id_pasajero) REFERENCES pasajeros (id_pasajero) ON UPDATE CASCADE ON DELETE NO ACTION
);

CREATE TABLE buses(
    id_bus VARCHAR(20) DEFAULT nextval('bus_seq' :: REGCLASS) NOT NULL,
    numero_interno_bus VARCHAR(20) NOT NULL UNIQUE,
    placa_bus VARCHAR(7) NOT NULL,
    tipo_bus VARCHAR(20) NOT NULL,
    id_ruta VARCHAR(20) NOT NULL,
    estado_bus VARCHAR(20) NOT NULL DEFAULT 'ACTIVO',
    CONSTRAINT bus_pkey PRIMARY KEY (id_bus) ,
    CONSTRAINT id_ruta_buses_fk FOREIGN KEY (id_ruta) REFERENCES rutas (id_ruta) ON UPDATE CASCADE ON DELETE NO ACTION
);

CREATE TABLE turnos(
    id_empleado VARCHAR(20) NOT NULL,
    id_bus VARCHAR(20) NOT NULL,
    fecha_inicio_turno DATE NOT NULL,
    fecha_fin_turno DATE NOT NULL,
    jornada_turno VARCHAR(50) NOT NULL,
    CONSTRAINT turnos_pk PRIMARY KEY (id_empleado,id_bus,fecha_inicio_turno,fecha_fin_turno,jornada_turno) ,
    CONSTRAINT id_empleado_turnos_fk FOREIGN KEY (id_empleado) REFERENCES empleados (id_empleado) ON UPDATE CASCADE ON DELETE NO ACTION,     
    CONSTRAINT id_bus_turnos_fk FOREIGN KEY (id_bus) REFERENCES buses (id_bus) ON UPDATE CASCADE ON DELETE NO ACTION
);


CREATE TABLE recargas(
	id_recarga VARCHAR(20) DEFAULT nextval('recargas_seq' :: REGCLASS) NOT NULL,
	pin_tarjeta VARCHAR(20),
	id_estacion  VARCHAR(20),	
	fecha TIMESTAMP NOT NULL,
	valor integer,
	CONSTRAINT recargas_pk PRIMARY KEY (id_recarga) ,
	CONSTRAINT pin_tarjeta_fk FOREIGN KEY (pin_tarjeta) REFERENCES tarjetas (pin_tarjeta) ON UPDATE CASCADE ON DELETE NO ACTION,
	CONSTRAINT estacion_fk FOREIGN KEY (id_estacion) REFERENCES estaciones (id_estacion) ON UPDATE CASCADE ON DELETE NO ACTION
);


--INSERTS 
--estaciones:
INSERT INTO estaciones (nombre_estacion,ubicacion_estacion,id_director,estado_estacion) 
VALUES ('SEDE-METROCALI','AV VASQUEZ COBO 23N 59','24','ACTIVO'),
('Universidades', 'Carrera 100,Callle 16','1','ACTIVO'),
('Pampalinda', 'Calle 5 Esquina Carrera 62','2','ACTIVO'), 
('Unidad deportiva', 'Calle 5 Carrera 52','3','ACTIVO'),
('Paso del Comercio','Carrera 1 Calle 71','16','ACTIVO'),
('Terminal Andres Sanin','Calle 75 Carrera 19','17','ACTIVO'),
('Capri','Calle 5 Carrera 78','18','ACTIVO'),
('Terminal Menga','Avenida 3N Calle 70','19','ACTIVO'),
('Calipso','Calle 36 Carrera 28D','20','ACTIVO'),
('Nuevo Latir','Carrera 28D Calle 83','21','ACTIVO'),
('Avenida de las Americas','Av 3N Calle 23AN','22','ACTIVO'),
('Torre de Cali','Avenida 3N Carrera 4N','23','ACTIVO');


--empleados:
INSERT INTO empleados (cedula_empleado, nombre_empleado,telefono_empleado,direccion_empleado,email_empleado,cargo_empleado,salario_empleado,id_estacion,estado_empleado,user_empleado,pass_empleado) 
VALUES ('1112475447', 'david ramirez','5559012','Av 9Oe 16N 32','davidramirez@metrocali.gov.co','DIRECTOR_ESTACION',1500000,'1','ACTIVO','dacer','dacer'),
('1114567889', 'Ivan Viveros','5553245','Av 77 # 32-05','ivan.v@metrocali.gov.co','DIRECTOR_ESTACION',1500000,'2','ACTIVO','12345','1234'),
('1112434678', 'Jose Libreros','3340654','Av 22e 16N 32','josel@metrocali.gov.co','DIRECTOR_ESTACION',1500000,'3','ACTIVO','12346','1234'),
('1155555555', 'Jose perez','3670654','Av 22e 16N 55','joseperez@metrocali.gov.co','DIRECTOR_OPERATIVO',1400000,'0','ACTIVO','12347','1234'),
('1112444448', 'julio franco','4435678','Av 33e 16N 32','juliof@metrocali.gov.co','AUXILIAR',1400000,'2','ACTIVO','12348','1234'),
('1112222278', 'andres corrido','3323454','Av 10e 16N 32','andresco@metrocali.gov.co','AUXILIAR',1400000,'1','ACTIVO','12349','1234'),
('1112222279', 'Bertha Solarte','3323424','Calle 103 25 32','bertha@metrocali.gov.co','AUXILIAR',1400000,'3','ACTIVO','12350','1234'),
('1112223279', 'Carlos Solarte','3323424','Calle 103 25 32','carlossolarte@metrocali.gov.co','AUXILIAR',1400000,'4','ACTIVO','12351','1234'),
('1112225279', 'Maria Gil','3323924','Calle 12 25 32','mariagil@metrocali.gov.co','AUXILIAR',1400000,'5','ACTIVO','12352','1234'),
('1144010021', 'Daniela Gonzales','3123424','Calle 23 25 32','danielagonzales@metrocali.gov.co','AUXILIAR',1400000,'6','ACTIVO','12353','1234'),
('1144010023', 'Marcela Caceres','3323454','Av 10e 16N 32','marcelacaceres@metrocali.gov.co','AUXILIAR',1400000,'7','ACTIVO','12354','1234'),
('13222222', 'Ligia Bonilla','3523424','Calle 11 25 32','ligiabonilla@metrocali.gov.co','AUXILIAR',1400000,'8','ACTIVO','12355','1234'),
('1144010022', 'Marcos Lopez','3723424','Carrera 27 25 32','marcoslopez@metrocali.gov.co','AUXILIAR',1400000,'9','ACTIVO','12356','1234'),
('1144010024', 'Tania Castillo','3823924','Av 9N 25 32','taniacastillo@metrocali.gov.co','AUXILIAR',1400000,'10','ACTIVO','12357','1234'),
('1144010025', 'Mariela Rodriguez','3923424','Carrera 23 25 32','marielarodriguez@metrocali.gov.co','AUXILIAR',1400000,'11','ACTIVO','12358','1234'),
('1112223888', 'Mateo Castillo','3323425','Calle 12B 25 32','mateocastillo@metrocali.gov.co','DIRECTOR_ESTACION',1400000,'4','ACTIVO','12359','1234'),
('1112225999', 'Maria Castanho','3323926','Calle 17H 25 32','mariacastanho@metrocali.gov.co','DIRECTOR_ESTACION',1400000,'5','ACTIVO','12360','1234'),
('1144010026', 'Gustavo Ramirez','3123484','Calle 22N 25 32','gustavoramirez@metrocali.gov.co','DIRECTOR_ESTACION',1400000,'6','ACTIVO','12361','1234'),
('1144010027', 'Cisto Duque','3323494','Av 10N 16N 32 Apto 22','cistoduque@metrocali.gov.co','DIRECTOR_ESTACION',1400000,'7','ACTIVO','12362','1234'),
('13222223', 'Esperanza Gordillo','3523404','Calle 111 25 32','esperanzagordillo@metrocali.gov.co','DIRECTOR_ESTACION',1400000,'8','ACTIVO','12363','1234'),
('1144010028', 'Juan Carlos Libreros','3723224','Carrera 111 25 32','carloslibreros@metrocali.gov.co','DIRECTOR_ESTACION',1400000,'9','ACTIVO','12364','1234'),
('1144010029', 'Diego Prado','3821924','Carrera 3 65 32','diegoprado@metrocali.gov.co','DIRECTOR_ESTACION',1400000,'10','ACTIVO','12365','1234'),
('1144010050', 'Angel Lopez','3920424','Carrera 27 75 32','angellopez@metrocali.gov.co','DIRECTOR_ESTACION',1400000,'11','ACTIVO','12366','1234'),
('13222244', 'Armando Garrido','3112233','Av 5Oe 5Oe 22','gerencia@metrocali.gov.co','GERENTE',14000000,'0','ACTIVO','12300','1234'),
('1122222800', 'Maria Solarte','3323424','Calle 103 25 32','mariasolarte@metrocali.gov.co','CONDUCTOR',1400000,'0','ACTIVO','12367','1234'),
('1122232801', 'Lucrecia Machado','3323424','Calle 103 25 32','lucreciamachado@metrocali.gov.co','CONDUCTOR',1400000,'0','ACTIVO','12368','1234'),
('1122252802', 'Juan Gil','3323924','Calle 12 25 32','juangil@metrocali.gov.co','CONDUCTOR',1400000,'0','ACTIVO','12369','1234'),
('1140100803', 'Pablo Gonzales','3123424','Calle 23 25 32','pablogonzales@metrocali.gov.co','CONDUCTOR',1400000,'0','ACTIVO','12370','1234'),
('1144010804', 'Lucas Caceres','3323454','Av 10e 16N 32','lucascaceres@metrocali.gov.co','CONDUCTOR',1400000,'0','ACTIVO','12371','1234'),
('13222280', 'Teodoro Bonilla','3523424','Calle 11 25 32','teodorobonilla@metrocali.gov.co','CONDUCTOR',1400000,'0','ACTIVO','12372','1234'),
('1140100805', 'Santiago Lopez','3723424','Carrera 27 25 32','santiagolopez@metrocali.gov.co','CONDUCTOR',1400000,'0','ACTIVO','12373','1234'),
('1144100806', 'Ana Castillo','3823924','Av 9N 25 32','anacastillo@metrocali.gov.co','CONDUCTOR',1400000,'0','ACTIVO','12374','1234'),
('1144000807', 'Sergio Rodriguez','3923424','Carrera 23 25 32','sergiorodriguez@metrocali.gov.co','CONDUCTOR',1400000,'0','ACTIVO','12375','1234'),
('1112228792', 'Claudia Castillo','3323425','Calle 12B 25 32','claudiacastillo@metrocali.gov.co','CONDUCTOR',1400000,'0','ACTIVO','12376','1234'),
('1112225817', 'Pastor Castanho','3323926','Calle 17H 25 32','pastorcastanho@metrocali.gov.co','CONDUCTOR',1400000,'0','ACTIVO','12377','1234'),
('11222374', 'Elias Ramirez','3123484','Calle 22N 25 32','eliasramirez@metrocali.gov.co','CONDUCTOR',1400000,'0','ACTIVO','12378','1234'),
('1144010827', 'Judas Duque','3323494','Av 10N 16N 32 Apto 22','judasduque@metrocali.gov.co','CONDUCTOR',1400000,'0','ACTIVO','12379','1234'),
('13222623', 'Campo Gordillo','3523404','Calle 111 25 32','campogordillo@metrocali.gov.co','CONDUCTOR',1400000,'0','ACTIVO','12380','1234'),
('1144010828', 'Telesforo Carlos Libreros','3723224','Carrera 111 25 32','telesforocarloslibreros@metrocali.gov.co','CONDUCTOR',1400000,'0','ACTIVO','12381','1234'),
('1144012929', 'Jairo Prado','3821924','Carrera 3 65 32','jairoprado@metrocali.gov.co','CONDUCTOR',1400000,'0','ACTIVO','12382','1234'),
('1144010282', 'Jose Lopez','3920424','Carrera 27 75 32','joselopez@metrocali.gov.co','CONDUCTOR',1400000,'0','ACTIVO','12383','1234'),
('1322224847', 'Brayan Garrido','3112233','Av 5Oe 5Oe 22','brayangarrido@metrocali.gov.co','CONDUCTOR',14000000,'0','ACTIVO','12384','1234');

--tarjetas:
INSERT INTO tarjetas (saldo_tarjeta,estado_tarjeta,estacion_id,fecha_venta)
VALUES (1800,'ACTIVO','2','2016-12-05 12:28:00'),
(3600,'ACTIVO','3','2016-11-05 11:28:00'),
(5400,'ACTIVO','4','2016-12-05 12:38:00'),
(-5400,'ACTIVO','4','2016-12-05 12:28:00'),
(5400,'ACTIVO','5','2016-10-05 14:28:00'),
(10400,'ACTIVO','2','2016-12-05 12:28:00'),
(55000,'INACTIVO','2','2016-12-05 16:28:00');
--pasajeros:
INSERT INTO pasajeros (cedula_pasajero,nombre_pasajero,telefono_pasajero,pin_tarjeta) 
VALUES ('12345678', 'cosme fulanito','5559013','4'),
('98712367', 'pepito perez','22234567','1'),
('54545446', 'juanita acosta','5559066','2'),
('12345555', 'cosme fulanito jr','5559013','3'),
('11143343', 'juan osa','4430693','5'),
('12332323', 'esteban quito','3325645','6'),
('12899898', 'alan brito','8890434','7');
--rutas:
INSERT INTO rutas (nombre_ruta,descripcion_recorrido) 
VALUES ('E31','Universidades-calle 5-chiminangos'),
('T31','Universidades-calle 5-chiminangos'),
('E37','Unidad Deportiva-calle 5-chiminangos'),
('T47B','Unidad Deportiva-calle 5-Carrera 15-Andres Sanin'),
('T40','Carrera 15-Calle 15-Calle 13-Centro'),
('T50','Transversal 29-Calle 15-Calle 13-Centro'),
('T57A','Transversal 29-Carrera 15-Calle 5-Unidad deportiva'),
('E21','Avenida 3N-Americas-Calle 13-Calle 5-Universidades'),
('E27','Avenida 3N-CAM-Comfenalco-Calle 5-Capri'),
('E27B','Avenida 3N-CAM-Comfenalco-Calle 5-Unidad Deportiva'),
('E52','Transversal 29-Calle 15-Avenida 3N-Terminal de Transportes'),
('T42','Calle 103-Carrera 15-Calle 15-Terminal de Transportes'),
('E41','Universidades-calle 5-belalcazar'),
('P71','Unidad Deportiva-Avenida Guadalupe-Caney-Unidad Deportiva'),
('P17','Unidad Deportiva-Calle 5-PUJ-ICESI'),
('A17B','Universidades-universidad autonoma-via puerto-Hormiguero-Cascajal-via jamundi-universidades'),
('A22','Terminal Menga-Barrio Brisas de los Alamos'); 

--Paradas de rutas en estaciones
INSERT INTO atender VALUES ('1','1'),('1','2'),('1','3'),('1','4'),('1','6'),('1','11'),
('2','1'),('2','2'),('2','3'),('2','4'),('2','6'),('2','11'),
('3','3'),('3','4'),('3','11'),
('4','3'),('4','5'),
('5','5'),
('6','8'),('6','9'),
('7','3'),('7','8'),('7','9'),
('8','1'),('8','2'),('8','3'),('8','6'),('8','7'),('8','10'),('8','11'),
('9','2'),('9','3'),('9','6'),('9','7'),('9','10'),
('10','3'),('10','7'),('10','10'),
('11','8'),('11','9'),('11','10'),('11','11'),
('12','10'),('12','11'),
('13','1'),('13','2'),('13','3'),('13','5'),
('14','3'),
('15','2'),('15','3'),('15','6'),
('16','1'),
('17','7');


--buses
INSERT INTO buses (numero_interno_bus,placa_bus,tipo_bus,id_ruta) 
VALUES ('MC11001','AAA-001','Articulado','1'),
 ('MC11002','AAA-002','Articulado','2'),
 ('MC21003','AAA-003','Articulado','3'),
 ('MC21004','AAA-004','Articulado','4'),
 ('MC11005','AAA-005','Articulado','5'),
 ('MC31006','AAA-006','Articulado','6'),
 ('MC11007','AAA-007','Articulado','7'),
 ('MC41008','AAA-008','Articulado','8'),
 ('MC41009','AAA-009','Articulado','13'),
 ('MC41010','AAA-010','Articulado','13'),
 ('MC12011','AAA-011','Padron','9'),
 ('MC12012','AAA-012','Padron','10'),
 ('MC12013','AAA-013','Padron','11'),
 ('MC12014','AAA-014','Padron','12'),
 ('MC22015','AAA-015','Padron','12'),
 ('MC22016','AAA-016','Padron','15'),
 ('MC22017','AAA-017','Padron','14'),
  ('MC32018','AAA-018','Padron','14'),
 ('MC32019','AAA-019','Padron','15'),
 ('MC42020','AAA-020','Padron','15'),
 ('MC13021','AAA-021','Complementaria','16'),
 ('MC13022','AAA-022','Complementaria','16'),
 ('MC23023','AAA-023','Complementaria','16'),
 ('MC33024','AAA-024','Complementaria','17'),
 ('MC43025','AAA-025','Complementaria','17');

--TURNOS
INSERT INTO turnos VALUES('25','1','2016-12-05','2016-12-05','6:00-10:00'),
('26','2','2016-12-05','2016-12-05','6:00-10:00'),
('27','3','2016-12-05','2016-12-05','6:00-10:00'),
('28','4','2016-12-05','2016-12-05','6:00-10:00'),
('29','5','2016-12-05','2016-12-05','6:00-10:00'),
('30','6','2016-12-05','2016-12-05','6:00-10:00'),
('31','7','2016-12-05','2016-12-05','6:00-10:00'),
('32','8','2016-12-05','2016-12-05','6:00-14:00'),
('33','9','2016-12-05','2016-12-05','6:00-14:00'),
('34','10','2016-12-05','2016-12-05','6:00-14:00'),
('35','11','2016-12-05','2016-12-05','6:00-14:00'),
('36','12','2016-12-05','2016-12-05','6:00-14:00'),
('37','13','2016-12-05','2016-12-05','6:00-14:00'),
('38','14','2016-12-05','2016-12-06','21:00-00:00'),
('39','15','2016-12-05','2016-12-06','21:00-00:00'),
('40','16','2016-12-05','2016-12-06','21:00-00:00'),
('41','17','2016-12-05','2016-12-05','6:00-14:00'),
('42','18','2016-12-05','2016-12-05','6:00-14:00'),
('25','19','2016-12-05','2016-12-05','10:00-14:00'),
('26','20','2016-12-05','2016-12-05','10:00-14:00'),
('27','21','2016-12-05','2016-12-05','10:00-14:00'),
('28','22','2016-12-05','2016-12-05','10:00-14:00'),
('29','23','2016-12-05','2016-12-05','10:00-14:00'),
('30','24','2016-12-05','2016-12-05','10:00-14:00'),
('31','25','2016-12-05','2016-12-05','10:00-14:00');

--Registro de pasajes
INSERT INTO registrar_pasaje VALUES 
('1','1','2016-12-05 12:28:00'),
('2','3','2016-11-05 12:28:27'),
('3','5','2016-12-05 12:28:29'),
('14','4','2016-11-05 12:29:00'),
('12','2','2016-12-05 12:28:12'),
('15','1','2016-10-05 12:28:12'),
('4','3','2016-12-04 16:28:24'),
('1','4','2016-12-05 17:28:40'),
('7','7','2016-12-01 18:28:00'),
('10','6','2016-10-05 18:28:00'),
('4','2','2016-12-29 12:28:00'),
('12','5','2016-10-05 10:11:37'),
('3','2','2016-10-05 12:28:00'),
('11','1','2016-12-05 21:52:00'),
('4','1','2016-12-24 14:28:00'),
('3','2','2016-12-05 07:17:00'),
('17','5','2016-11-05 10:02:13'),
('4','4','2016-12-05 11:26:00'),
('5','3','2016-12-05 09:24:00'),
('2','2','2016-12-27 06:33:00'),
('1','2','2016-12-22 07:22:00'),
('1','6','2016-11-05 08:24:00'),
('3','1','2016-11-12 11:26:00'),
('6','3','2016-11-05 14:28:00'),
('4','4','2016-11-05 14:27:00'),
('8','2','2016-12-01 14:19:00'),
('9','6','2016-12-02 14:18:00'),
('13','3','2016-12-03 15:31:52'),
('16','4','2016-12-04 16:57:12'),
('1','5','2016-12-05 17:55:29');

--Solicitudes PQRS
INSERT INTO solicitudPQRS (motivo_solicitud,descripcion_solicitud,fecha_solicitud,estado_solicitud,pasajero_id,estacion_id,id_empleado)
VALUES('Perdida','Al usuario se le extravio el celular en la estacion Universidades','2016-12-05','INICIADO','1','1','1'),
('Danho','Al usuario se le danho la tarjeta en la estacion Pampalinda','2016-12-01','EN_PROCESO','2','2','2'),
('Insulto','El usuario protagonizo pelea con conductor en estacion Universidades','2016-12-02','FINALIZADO','3','3','3');


INSERT INTO accionesPQRS (id_solicitud,accion_solicitud)
VALUES ('1','Hablar con vigilante'),
('1','Reponer tarjeta'),
('2','Cambio de tarjeta'),
('3','Hablar con el conductor');


INSERT INTO recargas(pin_tarjeta,id_estacion,fecha,valor)
VALUES ('1','1','2016-11-05 14:27:00',5000)
,('1','1','2016-11-05 14:27:00',5000)
,('1','2','2016-11-03 11:27:00',5000)
,('1','1','2016-10-05 12:27:00',5000)
,('2','3','2016-12-05 20:27:00',5000)
,('2','4','2016-11-05 22:27:00',5000)
,('2','3','2016-01-05 08:27:00',5000)
,('2','2','2016-11-05 11:27:00',5000)
;

--ALTER TABLES
--llaves:
ALTER TABLE empleados ADD CONSTRAINT estacion_fk FOREIGN KEY (id_estacion) REFERENCES estaciones (id_estacion);
ALTER TABLE estaciones ADD CONSTRAINT directos_estacion_fk FOREIGN KEY (id_director) REFERENCES empleados (id_empleado);
--checks:
ALTER TABLE empleados ADD CONSTRAINT chk_cargo_empleado CHECK 
(cargo_empleado = 'CONDUCTOR' OR cargo_empleado = 'AUXILIAR' OR cargo_empleado = 'DIRECTOR_OPERATIVO' OR cargo_empleado = 'DIRECTOR_ESTACION' OR cargo_empleado = 'GERENTE');
ALTER TABLE empleados ADD CONSTRAINT chk_estado_empleado CHECK (estado_empleado = 'ACTIVO' OR estado_empleado = 'INACTIVO');
ALTER TABLE buses ADD CONSTRAINT chk_estado_bus CHECK (estado_bus = 'ACTIVO' OR estado_bus = 'INACTIVO');
ALTER TABLE tarjetas ADD CONSTRAINT chk_estado_tarjeta CHECK (estado_tarjeta = 'ACTIVO' OR estado_tarjeta = 'INACTIVO');
ALTER TABLE tarjetas ADD CONSTRAINT chk_saldo_tarjeta CHECK (saldo_tarjeta >= -5400);
ALTER TABLE estaciones ADD CONSTRAINT chk_estado_estacion CHECK (estado_estacion = 'ACTIVO' OR estado_estacion = 'INACTIVO');
ALTER TABLE solicitudPQRS ADD CONSTRAINT chk_estado_solicitud CHECK (estado_solicitud = 'INICIADO' OR estado_solicitud = 'EN_PROCESO' OR estado_solicitud = 'FINALIZADO');
ALTER TABLE recargas ADD CONSTRAINT chk_recarga CHECK (valor>0);
