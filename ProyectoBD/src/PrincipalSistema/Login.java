/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PrincipalSistema;

import AuxiliaresDeServicio.InterfazAuxiliarServicio;
import Conductores.ConsultarTurnoConductor;
import DAO.controladores.ControladorEmpleado;
import DAO.logica.Empleado;
import DirectorOperativo.InterfazDirectorOperativo;
import InterfazEstacion.InterfazServicioAlCliente;
import InterfazGerente.InterfazGerente;
import javax.swing.JOptionPane;

/**
 *
 * @author JoseAlejandro
 */
public class Login extends javax.swing.JFrame {

    /**
     * Creates new form Login
     */
    ControladorEmpleado control;
    Empleado empleado;
    public Login() {
        this.control = new ControladorEmpleado();
        
        initComponents();
        this.setVisible(true);
        setLocationRelativeTo(null);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jpfContrasenha = new javax.swing.JPasswordField();
        jftUsuario = new javax.swing.JFormattedTextField();
        jbIngresar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(325, 300));
        setPreferredSize(new java.awt.Dimension(325, 300));
        setResizable(false);
        getContentPane().setLayout(null);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/login.png"))); // NOI18N

        jLabel2.setText("Usuario");

        jLabel3.setText("Contraseña");

        jbIngresar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/bIngresar.png"))); // NOI18N
        jbIngresar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jbIngresarMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jbIngresar, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(14, 14, 14))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jftUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, 112, Short.MAX_VALUE)
                        .addComponent(jpfContrasenha)))
                .addGap(102, 102, 102))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(132, 132, 132)
                        .addComponent(jLabel3))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(142, 142, 142)
                        .addComponent(jLabel2)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jftUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jpfContrasenha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jbIngresar, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 68, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 320, 320);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jbIngresarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jbIngresarMouseClicked
        String user = jftUsuario.getText().trim();
        String pass = String.valueOf(jpfContrasenha.getPassword());
        
        if (user.length()>0 && pass.length()>0){
            
           try {
            this.empleado= control.consultarEmpleadoUser(user);
            
            
            
            if(this.empleado.getUsuario().equals(user) && this.empleado.getContrasenha().equals(pass)) {
                JOptionPane.showMessageDialog(rootPane, "bienvenido: "+this.empleado.getNombre(), "Login", JOptionPane.INFORMATION_MESSAGE);
                String cargo = this.empleado.getCargo();
                if(cargo.equalsIgnoreCase("CONDUCTOR")) {
                    ConsultarTurnoConductor conductor = new ConsultarTurnoConductor(this.empleado.getId());
                    conductor.setVisible(true);
                
                }else if (cargo.equalsIgnoreCase("AUXILIAR")) {
                    
                    InterfazAuxiliarServicio servicio = new InterfazAuxiliarServicio(empleado.getIdEstacion());
                    servicio.setVisible(true);
                
                }else if(cargo.equalsIgnoreCase("DIRECTOR_OPERATIVO")){
                    new InterfazDirectorOperativo().setVisible(true);
                }else if(cargo.equalsIgnoreCase("DIRECTOR_ESTACION")){
                   new InterfazServicioAlCliente().setVisible(true);
                
                }else if(cargo.equalsIgnoreCase("GERENTE")){
                  InterfazGerente gerente = new InterfazGerente();
                          gerente.setVisible(true);
                }
                
                
                
                this.dispose();
                 
            }else if(this.empleado.getUsuario().equals("") || this.empleado == null){
             JOptionPane.showMessageDialog(rootPane, "Ingrese un usuario y contraseña valido", "Login", JOptionPane.INFORMATION_MESSAGE);
    
            }else {
            JOptionPane.showMessageDialog(rootPane, "Ingrese un usuario y contraseña valido", "Login", JOptionPane.INFORMATION_MESSAGE);
    
            }
        
            }catch (NullPointerException ex){
                JOptionPane.showMessageDialog(rootPane, "Ingrese un usuario y contraseña valido", "Login", JOptionPane.INFORMATION_MESSAGE);
    
            }
        }else{
            JOptionPane.showMessageDialog(rootPane, "Ingrese un usuario y contraseña", "Login", JOptionPane.INFORMATION_MESSAGE);
    
        }
        
        
        
    }//GEN-LAST:event_jbIngresarMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Login().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JButton jbIngresar;
    private javax.swing.JFormattedTextField jftUsuario;
    private javax.swing.JPasswordField jpfContrasenha;
    // End of variables declaration//GEN-END:variables
}

