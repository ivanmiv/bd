/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.controladores;

import DAO.logica.Atender;
import DAO.objetos.DaoAtender;
import javax.swing.JOptionPane;

/**
 *
 * @author dacer
 */
public class ControladorAtender {
    DaoAtender daoAtender;

    public ControladorAtender() {
    
        this.daoAtender = new DaoAtender();
    }
    
    
    public void  insertarAtender(String ruta,String estacion){
        
        Atender atender = new Atender();       
        
        atender.setIdRuta(ruta);
        atender.setIdEstacion(estacion);
        
        System.out.println("Se va a insertar una atender");
        
        String result =daoAtender.IngresarAtender(atender);

        System.out.println(result);
    }
     
    public Atender consultarAtender(String ruta, String estacion){
        

        Atender atender = new Atender();
        
         System.out.println("Se va a consultar un atender");

        atender = daoAtender.ConsultarAtender(ruta,estacion);
      
       return atender;
    }
    
     public void  actualizarAtender(String ruta, String estacion, String estacionvieja){
        
        Atender atender = new Atender();       
        
        atender.setIdRuta(ruta);
        atender.setIdEstacion(estacion);
        
        System.out.println("Se va a actualizar una atender");
        
        String result = daoAtender.ActualizarAtender(atender,ruta,estacion);
        
        JOptionPane.showMessageDialog(null, result);
    }
     
    public void EliminarAtender(String ruta, String estacion){
        Atender atender = new Atender();       
        
        atender.setIdRuta(ruta);
        atender.setIdEstacion(estacion);
        
        System.out.println("Se va a actualizar una atender");
        
        String result = daoAtender.EliminarAtender(atender);
        
        System.out.println(result);
    
    }
    
}
