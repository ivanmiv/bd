/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.controladores;

import DAO.logica.Pasajero;
import DAO.objetos.DaoPasajeros;
import javax.swing.JOptionPane;

/**
 *
 * @author flavio
 */
public class ControladorPasajero {
    DaoPasajeros daoPasajeros;

    public ControladorPasajero() {
        this.daoPasajeros = new DaoPasajeros();
    }
    
    public void  insertarPasajero( String cedula,String nombre, String telefono, String tarjeta){
        
        Pasajero pasajero = new Pasajero();        

        pasajero.setCedula(cedula);
        pasajero.setNombre(nombre);
        pasajero.setTelefono(telefono);
        pasajero.setTarjeta(tarjeta);
        
        System.out.println("Se va a insertar un pasajero");
        
        String result =daoPasajeros.IngresarPasajero(pasajero);

        JOptionPane.showMessageDialog(null, result);
    }
    
     public Pasajero consultarPasajero(String codigo){
        

        Pasajero pasajero = new Pasajero();
        
         System.out.println("Se va a consultar un pasajero");

        pasajero = daoPasajeros.ConsultarPasajero(codigo);
      
       return pasajero;
    }
     
     public void  actualizarPasajero(String codigo ,String cedula,String nombre, String telefono, String tarjeta){
        
        Pasajero pasajero = new Pasajero();        
        
        pasajero.setId(codigo);
        pasajero.setCedula(cedula);
        pasajero.setNombre(nombre);
        pasajero.setTelefono(telefono);
        pasajero.setTarjeta(tarjeta);
               
        System.out.println("Se va a actualizar un pasajero");
        
        String result =daoPasajeros.ActualizarPasajero(pasajero);
        
        JOptionPane.showMessageDialog(null, result);
    }
     
     public boolean comprobarTarjetaNoRegistradaOtroUsuario(String pin){
         boolean estaRegistrada = false;
        
        System.out.println("Se va a chequear una tarjeta");
        
        int result =daoPasajeros.comprobarTarjetaNoRegistradaOtroUsuario(pin);
        if(result == 1){
            return true;
        }else{
            return false;
        }

     }
     public Pasajero consultarPasajeroCedula(String cedula){
        

        Pasajero empleado = new Pasajero();
        
         System.out.println("Se va a consultar un pasajero");

        empleado = daoPasajeros.ConsultarPasajeroCedula(cedula);
       System.out.println(empleado.getId());
       return empleado;
    }
     
     
     public Pasajero consultarPasajeroPorTarjeta(String pin_tarjeta){
         Pasajero pasajero = daoPasajeros.consultarPasajeroPorTarjeta(pin_tarjeta);
         return pasajero;
     }

    public String rutasFrecuentes(String id_pasajero) {
        String rutasFrecuentes = null;
        rutasFrecuentes = daoPasajeros.rutasFrecuentes(id_pasajero);
        return rutasFrecuentes;
    }
}
