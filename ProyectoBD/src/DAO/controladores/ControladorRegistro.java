/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.controladores;

import DAO.logica.RegistrarPasaje;
import DAO.objetos.DaoRegistrarPasaje;
import javax.swing.JOptionPane;

/**
 *
 * @author dacer
 */
public class ControladorRegistro {
    DaoRegistrarPasaje daoRegistrarPasaje;

    public ControladorRegistro() {
        this.daoRegistrarPasaje = new DaoRegistrarPasaje();
    
    }
    
    
    public void  insertarRegistrarPasaje(String ruta,String pasajero,String fecha){
        
        RegistrarPasaje registrarPasaje = new RegistrarPasaje();       
        
        registrarPasaje.setRuta(ruta);
        registrarPasaje.setPasajero(pasajero);
        registrarPasaje.setFecha(fecha);
        
        System.out.println("Se va a insertar una registrarPasaje");
        
        String result =daoRegistrarPasaje.IngresarRegistrarPasaje(registrarPasaje);

        JOptionPane.showMessageDialog(null, result);
    }
     
    public RegistrarPasaje consultarRegistrarPasaje(String ruta, String pasajero){
        

        RegistrarPasaje registrarPasaje = new RegistrarPasaje();
        
         System.out.println("Se va a consultar un registrarPasaje");

        registrarPasaje = daoRegistrarPasaje.ConsultarRegistrarPasaje(ruta,pasajero);
      
       return registrarPasaje;
    }
    
     public void  actualizarRegistrarPasaje(String ruta, String pasajero, String fecha){
        
        RegistrarPasaje registrarPasaje = new RegistrarPasaje();       
        
        registrarPasaje.setRuta(ruta);
        registrarPasaje.setPasajero(pasajero);
        registrarPasaje.setFecha(fecha);
        
        System.out.println("Se va a actualizar una registrarPasaje");
        
        String result = daoRegistrarPasaje.ActualizarRegistrarPasaje(registrarPasaje,ruta,pasajero);
        
        JOptionPane.showMessageDialog(null, result);
    }
    
}
