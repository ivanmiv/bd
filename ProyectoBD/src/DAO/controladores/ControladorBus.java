/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.controladores;

import DAO.logica.Bus;
import DAO.objetos.DaoBus;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author JoseAlejandro
 */
public class ControladorBus {
    DaoBus daoBus = new DaoBus();
    public ControladorBus() {
        
    }
    
    public String RegistrarBus(Bus bus){
        return daoBus.IngresarBus(bus);
    }

    

    public Bus buscarBusPorPlaca(String placa) {
        return daoBus.consultarBusPlaca(placa);
    }
    public Bus buscarBus(String placa) {
        return daoBus.ConsultarBus(placa);
    }
    
    public Bus buscarBusNumeroInterno(String numeroInterno) {
        return daoBus.consultaBusNumeroInterno(numeroInterno);
    }

    public ArrayList<Bus> buscarTodosBuses() {
       return daoBus.extraerTodosBuses(); 
    }

    public ArrayList<Bus> buscarBusesTipologia(String tipologia) {
        return daoBus.consultaBusesTipologia(tipologia);
    }

    public String modificarRutaBus(String placa, String numeroInterno,String id_ruta) {
        return daoBus.actualizarRuta(placa,numeroInterno,id_ruta);
    }
    
     public void TraerBusesTabla(JTable tabla){  
        
        
        ArrayList<ArrayList<Object>> informacion = new ArrayList();
        ResultSet datos = daoBus.TreaerBuses();

        DefaultTableModel modelo = new DefaultTableModel(){

            @Override
            public boolean isCellEditable(int row, int column) {
                //Only the third column
                return false;
            }
         };
        modelo.setColumnIdentifiers(new String[]{
            "Numero bus", "tipo bus"
        });

        try {
            while (datos.next()) {
                //Cuadrar tabla para cualquier cantidad de usuarios

                ArrayList<Object> datosFila = new ArrayList();

                datosFila.add(datos.getString("numero_interno_bus"));
                datosFila.add(datos.getString("tipo_bus"));
                
                modelo.addRow(datosFila.toArray());

            }

        } catch (Exception e) {
            System.err.println(e);
            e.printStackTrace();
        }
        
        tabla.setModel(modelo);
    }
     
    public Bus ConsultarBusNUmeroInterno(String idbus){
        Bus bus = new Bus();
         System.out.println("Se va a consultar un bus");
        
         bus = daoBus.ConsultarBusNUmeroInterno(idbus);
        return bus;
    
    }

    public ResultSet consultarBusesArticuladosConductores() {
        return daoBus.consultarBusesArtculadosConductores(); 
    }
}
