/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.controladores;

import DAO.logica.Ruta;
import DAO.objetos.DaoRuta;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author flavio
 */
public class ControladorRuta {
    DaoRuta daoRuta;

    public ControladorRuta() {
        this.daoRuta =  new DaoRuta();
    }
    
     public void  insertarRuta(String nombre,String recorrido){
        
        Ruta ruta = new Ruta();       
        
        ruta.setNombre(nombre);
        ruta.setDesscripcion(recorrido);
        
        System.out.println("Se va a insertar una ruta");
        
        String result =daoRuta.IngresarRuta(ruta);

        JOptionPane.showMessageDialog(null, result);
    }
     
    public Ruta consultarRuta(String codigo){
        

        Ruta ruta = new Ruta();
        
         System.out.println("Se va a consultar un ruta");

        ruta = daoRuta.ConsultarRuta(codigo);
      
       return ruta;
    }
    public Ruta consultarRutaNombre(String nombre){
        

        Ruta ruta = new Ruta();
        
         System.out.println("Se va a consultar un ruta");

        ruta = daoRuta.ConsultarRutaNombre(nombre);
      
       return ruta;
    }
    
     public void  actualizarRuta(String identificacion, String nombre, String descrip){
        
        Ruta ruta = new Ruta();       
        
        ruta.setId(identificacion);
        ruta.setNombre(nombre);
        ruta.setDesscripcion(descrip);
        
        System.out.println("Se va a actualizar una ruta");
        
        String result = daoRuta.ActualizarRuta(ruta);
        
        JOptionPane.showMessageDialog(null, result);
    }
     
    public ArrayList<Ruta> consultarRutas(){
        ArrayList<Ruta> rutasExistentes = new ArrayList<>();
        System.out.println("Se va a consultar un ruta");
        
        rutasExistentes = daoRuta.ConsultarRutas();
        return rutasExistentes;
    }
    
     
    
    
    public String consultarParadas(String identificacion){
        String paradas = daoRuta.consultarParadas(identificacion);        
        return paradas;
    }
    
    
    public ResultSet consultarNombreDescripcionRuta(){
        return daoRuta.consultarNombreDescripcionRuta();        
      
    }

    public ArrayList<Ruta> hacerPlanViaje(String id_estacionFrom, String id_estacionTo) {
        return daoRuta.consultarEstacionesFromTo(id_estacionFrom,id_estacionTo);
    }

    
    
    
}
