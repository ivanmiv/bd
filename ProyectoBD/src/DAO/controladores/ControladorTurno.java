/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.controladores;

import DAO.logica.Turno;
import DAO.objetos.DaoTurno;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author dacer
 */
public class ControladorTurno {
    DaoTurno daoTurno;
    public ControladorTurno() {
        this.daoTurno = new DaoTurno();
    }
    
    
    
   public void  IngresarTurno( String empleado, String bus, String fechaInicio, String fechaFin,String jornada ){
        
        Turno turno = new Turno();        

        turno.setEmpleado(empleado);
        turno.setBus(bus);
        turno.setFechaInicio(fechaInicio);
        turno.setFechaFin(fechaFin);
        turno.setJornada(jornada);
        
        
        System.out.println("Se va a insertar un turno");
        
        String result =daoTurno.IngresarTurno(turno);

        JOptionPane.showMessageDialog(null, result);
    }
   
   public  void ActualizarTurno(String nuevobus,String nuevoinicio,String nuevofin, String nuevajornada
           ,String empleado, String bus, String inicio, String fin, String jornada){
       
       
       Turno turnooriginal =  new Turno();
       Turno turnocambiado =  new Turno();
       
       turnooriginal.setEmpleado(empleado);
       turnooriginal.setBus(bus);
       turnooriginal.setFechaInicio(inicio);
       turnooriginal.setFechaFin(fin);
       turnooriginal.setJornada(jornada);
       
       turnocambiado.setEmpleado(empleado);
       turnocambiado.setBus(nuevobus);
       turnocambiado.setFechaInicio(nuevoinicio);
       turnocambiado.setFechaFin(nuevofin);
       turnocambiado.setJornada(nuevajornada);
       
       System.out.println("Se va a actualizar un turno");
       
       String result =daoTurno.ActualizarTurno(turnooriginal,turnocambiado);
        
        JOptionPane.showMessageDialog(null, result);
       
   
   }
   
   
   public void TraerTurnosTabla(JTable tabla){  
        
        
        ArrayList<ArrayList<Object>> informacion = new ArrayList();
        ResultSet datos = daoTurno.TraerTunos();

        DefaultTableModel modelo = new DefaultTableModel(){

            @Override
            public boolean isCellEditable(int row, int column) {
                //Only the third column
                return false;
            }
         };
        modelo.setColumnIdentifiers(new String[]{
            "Nombre", "cédula","Bus","Inicio","fin","jornada"
        });

        try {
            while (datos.next()) {
                //Cuadrar tabla para cualquier cantidad de usuarios

                ArrayList<Object> datosFila = new ArrayList();

                datosFila.add(datos.getString("nombre_empleado"));
                datosFila.add(datos.getString("cedula_empleado"));
                datosFila.add(datos.getString("numero_interno_bus"));
                datosFila.add(datos.getString("fecha_inicio_turno"));
                datosFila.add(datos.getString("fecha_fin_turno"));
                datosFila.add(datos.getString("jornada_turno"));
               
                modelo.addRow(datosFila.toArray());

            }

        } catch (Exception e) {
            System.err.println(e);
            e.printStackTrace();
        }
        
        tabla.setModel(modelo);
    }
   
   public Turno traerultimoturnoEmpleado(String empleado){
        Turno turno = new Turno();
        
         System.out.println("Se va a consultar un empleado");

        turno  = daoTurno.ConsultarturnoUser(empleado);
       
       return turno;
   }
}
