/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.controladores;

import DAO.logica.Estacion;
import DAO.objetos.DaoEstaciones;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author flavio
 */
public class ControladorEstacion {
    DaoEstaciones daoEstaciones;

    public ControladorEstacion() {
        this.daoEstaciones =  new DaoEstaciones();
    }
    
    public void  insertarEstacion( String nombre, String ubicacion, String director, String estado){
        
        Estacion estacion = new Estacion();        

        estacion.setNombre(nombre);
        estacion.setUbicacion(ubicacion);
        estacion.setDirector(director);
        estacion.setEstado(estado);
        
        System.out.println("Se va a insertar una estacion");
        
        String result =daoEstaciones.IngresarEstacion(estacion);

        JOptionPane.showMessageDialog(null, result);
    }
    
    public Estacion consultarEstacion(String codigo){
        

        Estacion estacion = new Estacion();
        
         System.out.println("Se va a consultar un estacion");

        estacion = daoEstaciones.ConsultarEstacion(codigo);
      
       return estacion;
    }
    
    public Estacion consultarEstacionNombre(String nombre){
        

        Estacion estacion = new Estacion();
        
         System.out.println("Se va a consultar un estacion");

        estacion = daoEstaciones.ConsultarEstacionNombre(nombre);
      
       return estacion;
    }
    
    public void  actualizarEstacion(String codigo, String nombre, String ubicacion, String director, String estado){
        
        Estacion estacion = new Estacion();       
        
        estacion.setId(codigo);
        estacion.setNombre(nombre);
        estacion.setUbicacion(ubicacion);
        estacion.setDirector(director);
        estacion.setEstado(estado);
        
        System.out.println("Se va a actualizar una estacion");
        
        String result = daoEstaciones.ActualizarEstacion(estacion);
        
        System.out.println(result);
    }
    
    public void MostrarEstaciones(JList tabla){
        ArrayList<Estacion> estaciones = daoEstaciones.MostrarEstaciones();
        Estacion estaciontemp;
        Iterator<Estacion> nombreIterator = estaciones.iterator();
        DefaultListModel modelo = new DefaultListModel();
                while(nombreIterator.hasNext()){
                    estaciontemp = nombreIterator.next();
                  
                   modelo.addElement(estaciontemp.getNombre());
                }
        tabla.setModel(modelo);
    }
    
    public void MostrarEstacionesTabla(JTable tabla){
        ArrayList<Estacion> estaciones = daoEstaciones.MostrarEstaciones();
        Estacion estaciontemp;
        Iterator<Estacion> nombreIterator = estaciones.iterator();
        
        DefaultTableModel modelo = new DefaultTableModel(){

            @Override
            public boolean isCellEditable(int row, int column) {
                //Only the third column
                return false;
            }
         };
        modelo.setColumnIdentifiers(new String[]{
            "Nombre", "identificador"
        });
        
        
        while (nombreIterator.hasNext()) {
                //Cuadrar tabla para cualquier cantidad de usuarios
            estaciontemp = nombreIterator.next();
                ArrayList<Object> datosFila = new ArrayList();

                datosFila.add(estaciontemp.getNombre());
                datosFila.add(estaciontemp.getId());
                
               
                modelo.addRow(datosFila.toArray());

            }
        

        tabla.setModel(modelo);
    }
    public ArrayList<String> MostrarEstacionesRuta(JList tabla,String nombreRuta){
        
        ArrayList<Estacion> estaciones = daoEstaciones.MostrarEstacionesRuta(nombreRuta);
        ArrayList<String> nombres = new ArrayList<>();
        Estacion estaciontemp;
        Iterator<Estacion> nombreIterator = estaciones.iterator();
        DefaultListModel modelo = new DefaultListModel();
                while(nombreIterator.hasNext()){
                    estaciontemp = nombreIterator.next();
                  
                   modelo.addElement(estaciontemp.getNombre());
                   nombres.add(estaciontemp.getNombre());
                }
        tabla.setModel(modelo);
        return nombres;
    }
    
    public ArrayList<Estacion> cargarEstaciones(){
        ArrayList<Estacion> estaciones = daoEstaciones.MostrarEstaciones();
        return estaciones;
    }
            
}
