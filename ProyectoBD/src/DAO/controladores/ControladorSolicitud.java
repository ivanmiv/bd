/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.controladores;

import DAO.logica.Solicitud;
import DAO.objetos.DaoAccion;
import DAO.objetos.DaoSolicitud;
import java.sql.ResultSet;
import javax.swing.JOptionPane;

/**
 *
 * @author dacer
 */
public class ControladorSolicitud {
    DaoSolicitud daoSolicitud;
    DaoAccion daoAccion;

    public ControladorSolicitud() {
        this.daoSolicitud = new DaoSolicitud();
        this.daoAccion = new DaoAccion();
    }
    
    public void  insertarSolicitud(String motivo, String idescripcion, String fechaSolicitud, String estado,
                        String pasajero, String estacion){
        
        Solicitud solicitud = new Solicitud();        

        solicitud.setMotivo(motivo);
        solicitud.setIdescripcion(idescripcion);
        solicitud.setFechaSolicitud(fechaSolicitud);
        solicitud.setEstado(estado);
        //solicitud.addAccion(accion);
        solicitud.setPasajero(pasajero);
        solicitud.setEstacion(estacion);
        
        System.out.println("Se va a insertar una solicitud");
        
        String result =daoSolicitud.IngresarSolicitud(solicitud);

        JOptionPane.showMessageDialog(null, result);
    }
    
    public Solicitud consultarSolicitud(String codigo){
        

        Solicitud solicitud;
        
         System.out.println("Se va a consultar un solicitud");

        solicitud = daoSolicitud.ConsultarSolicitud(codigo);
      
       return solicitud;
    }
    
    public void  actualizarSolicitud(String codigo, String motivo, String idescripcion, String fechaSolicitud, String estado, String accion,
                        String pasajero, String estacion){
        
        Solicitud solicitud = new Solicitud();       
        
        solicitud.setId(codigo);
        solicitud.setMotivo(motivo);
        solicitud.setIdescripcion(idescripcion);
        solicitud.setFechaSolicitud(fechaSolicitud);
        solicitud.setEstado(estado);
        //solicitud.addAccion(accion);
        solicitud.setPasajero(pasajero);
        solicitud.setEstacion(estacion);
        
        System.out.println("Se va a actualizar una solicitud");
        
        String result = daoSolicitud.ActualizarSolicitud(solicitud);
        
        JOptionPane.showMessageDialog(null, result);
    }

    public String traerMaximoSolicitudes() {
        String max;
        max =daoSolicitud.traerMaximoSolicitudes();
        return max;
    }

    public void ingresarAccion(String ticket, String Accion) {
        
        String result =daoAccion.IngresarAccion(ticket,Accion);

        JOptionPane.showMessageDialog(null, result);
    }
    
    public String TraerSolicitudReversa(String motivo , String fecha, String pasajero){
        
        
        String solicitud="";
        
         System.out.println("Se va a consultar un solicitud");

        solicitud = daoSolicitud.TraerSolicitudReversa(motivo, fecha, pasajero);
      
       return solicitud;
    
    
    
    }

    public ResultSet consultarSolicitudesComunes() {
        return daoSolicitud.consultarSolicitudesComunes();
    }
    
    
}
