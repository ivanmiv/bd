
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.controladores;

import DAO.logica.Empleado;
import DAO.objetos.DaoEmpleado;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author flavio
 */
public class ControladorEmpleado {

    DaoEmpleado daoEmpleado;

    public ControladorEmpleado() {
        daoEmpleado = new DaoEmpleado();
    }

    public void insertarEmpleado(String cedula, String nombre, String telefono, String direccion, String email,
            String cargo, int salario, String idEstacion, String estado, String usuario, String contrasenha) {

        Empleado empleado = new Empleado();

        empleado.setCedula(cedula);
        empleado.setNombre(nombre);
        empleado.setTelefono(telefono);
        empleado.setDireccion(direccion);
        empleado.setEmail(email);
        empleado.setCargo(cargo);
        empleado.setSalario(salario);
        empleado.setIdEstacion(idEstacion);
        empleado.setEstado(estado);
        empleado.setUsuario(usuario);
        empleado.setContrasenha(contrasenha);

        System.out.println("Se va a insertar un empleado");

        String result = daoEmpleado.IngresarEmpleado(empleado);

        JOptionPane.showMessageDialog(null, result);
    }

    public Empleado consultarEmpleado(String codigo) {

        Empleado empleado = new Empleado();

        System.out.println("Se va a consultar un empleado");

        empleado = daoEmpleado.ConsultarEmpleado(codigo);

        return empleado;
    }

    public Empleado consultarEmpleadoCedula(String cedula) {

        Empleado empleado = new Empleado();

        System.out.println("Se va a consultar un empleado");

        empleado = daoEmpleado.ConsultarEmpleadoCedula(cedula);

        return empleado;
    }

    public Empleado consultarEmpleadoUser(String usuario) {

        Empleado empleado = new Empleado();

        System.out.println("Se va a consultar un empleado");

        empleado = daoEmpleado.ConsultarEmpleadoUser(usuario);

        return empleado;
    }

    public void TraerConductoresTabla(JTable tabla) {

        ArrayList<ArrayList<Object>> informacion = new ArrayList();
        ResultSet datos = daoEmpleado.TreaerConductores();

        DefaultTableModel modelo = new DefaultTableModel() {

            @Override
            public boolean isCellEditable(int row, int column) {
                //Only the third column
                return false;
            }
        };
        modelo.setColumnIdentifiers(new String[]{
            "Nombre", "Cédula"
        });

        try {
            while (datos.next()) {
                //Cuadrar tabla para cualquier cantidad de usuarios

                ArrayList<Object> datosFila = new ArrayList();

                datosFila.add(datos.getString("nombre_empleado"));
                datosFila.add(datos.getString("cedula_empleado"));

                modelo.addRow(datosFila.toArray());

            }

        } catch (Exception e) {
            System.err.println(e);
            e.printStackTrace();
        }

        tabla.setModel(modelo);
    }

    public void actualizarEmpleado(String codigo, String cedula, String nombre, String telefono, String direccion, String email,
            String cargo, int salario, String idEstacion, String estado) {

        Empleado empleado = new Empleado();

        empleado.setId(codigo);
        empleado.setCedula(cedula);
        empleado.setNombre(nombre);
        empleado.setTelefono(telefono);
        empleado.setDireccion(direccion);
        empleado.setEmail(email);
        empleado.setCargo(cargo);
        empleado.setSalario(salario);
        empleado.setIdEstacion(idEstacion);
        empleado.setEstado(estado);

        System.out.println("Se va a actualizar un empleado");

        String result = daoEmpleado.ActualizarEmpleado(empleado);

        JOptionPane.showMessageDialog(null, result);
    }

    public void TraerAsesoresEstacionTabla(String estacion, JTable tabla) {

        ArrayList<ArrayList<Object>> informacion = new ArrayList();
        ResultSet datos = daoEmpleado.TreaerJefeEstacion(estacion);

        DefaultTableModel modelo = new DefaultTableModel() {

            @Override
            public boolean isCellEditable(int row, int column) {
                //Only the third column
                return false;
            }
        };
        modelo.setColumnIdentifiers(new String[]{
            "Nombre", "Cédula"
        });

        try {
            while (datos.next()) {
                //Cuadrar tabla para cualquier cantidad de usuarios

                ArrayList<Object> datosFila = new ArrayList();

                datosFila.add(datos.getString("nombre_empleado"));
                datosFila.add(datos.getString("cedula_empleado"));

                modelo.addRow(datosFila.toArray());

            }

        } catch (Exception e) {
            System.err.println(e);
            e.printStackTrace();
        }

        tabla.setModel(modelo);

    }


}
