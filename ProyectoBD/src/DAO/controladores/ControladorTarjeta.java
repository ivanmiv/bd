/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.controladores;

import DAO.logica.Tarjeta;
import DAO.objetos.DaoTarjeta;
import javax.swing.JOptionPane;

/**
 *
 * @author flavio
 */
public class ControladorTarjeta {
    DaoTarjeta daoTarjeta;

    public ControladorTarjeta() {
        this.daoTarjeta = new DaoTarjeta();
    }
    
     public void  insertarTarjeta(int saldo,String estado, String id_estacion){
        
        Tarjeta tarjeta = new Tarjeta();       
        
        tarjeta.setSaldo(saldo);
        tarjeta.setEstado(estado);
        tarjeta.setEstacion_id_creacion(id_estacion);
        
        
        System.out.println("Se va a insertar una tarjeta");
        
        String result =daoTarjeta.IngresarTarjeta(tarjeta);

        //JOptionPane.showMessageDialog(null, result);
    }
     
     public String obtenerMaximoRegistroTarjeta(){
         String mensaje = daoTarjeta.obtenerMaximoRegistroTarjeta();
         return mensaje;
     }
     
    public Tarjeta consultarTarjeta(String codigo){
        

        Tarjeta tarjeta = new Tarjeta();
        
         System.out.println("Se va a consultar un tarjeta");

        tarjeta = daoTarjeta.ConsultarTarjeta(codigo);
      
       return tarjeta;
    }
    
     public void  actualizarTarjeta(String pin, int saldo, String estado){
        
        Tarjeta tarjeta = new Tarjeta();       
        
        tarjeta.setPin(pin);
        tarjeta.setSaldo(saldo);
        tarjeta.setEstado(estado);
        
        System.out.println("Se va a actualizar una tarjeta");
        
        String result = daoTarjeta.ActualizarTarjeta(tarjeta);
        
        JOptionPane.showMessageDialog(null, result);
    }
     
     
    
}
