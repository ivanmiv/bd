/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.objetos;

import AccesoDatos.Fachada;
import DAO.logica.RegistrarPasaje;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author dacer
 */
public class DaoRegistrarPasaje {
    Fachada fachada;

    public DaoRegistrarPasaje() {
    
        this.fachada =  new Fachada();
    }
    
    public String IngresarRegistrarPasaje(RegistrarPasaje registrarPasaje){
        String mensaje="",query;
        int resultado;
        query= "INSERT INTO registrar_pasaje (id_ruta,id_pasajero,fecha_hora )"
                + "VALUES ('" +registrarPasaje.getRuta()+ "', '" +registrarPasaje.getPasajero()+"', '"+ registrarPasaje.getFecha()+"');";
        
        
        try {
            Connection conexion = this.fachada.getConexion();
            Statement sentencia = conexion.createStatement();
            resultado = sentencia.executeUpdate(query);
            if (resultado == 1) {
                mensaje = "Registro ingresado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar el registro :" + ex.getMessage();
        }
    
        return mensaje;
    }
    
    public RegistrarPasaje ConsultarRegistrarPasaje(String ruta,String pasajero){
        
        RegistrarPasaje registrarPasaje= new RegistrarPasaje();
        String sql_select;
        sql_select="SELECT id_ruta,id_pasajero,fecha_hora "
                + "FROM  registrar_pasaje WHERE id_ruta='" + ruta + "' AND id_pasajero='"+pasajero+"';";
         try{
            Connection conn= fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql_select);
            
            while(resultado.next()){
                
               registrarPasaje.setRuta(resultado.getString(1));
               registrarPasaje.setPasajero(resultado.getString(2));
               registrarPasaje.setFecha(resultado.getString(3));
              
            }
           
            return registrarPasaje;
         }
         catch(SQLException e){ 
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en registrarPasaje",JOptionPane.ERROR_MESSAGE );
         }
         catch(Exception e){
            JOptionPane.showMessageDialog(null, "Error desconocido", "Error en registrarPasaje",JOptionPane.ERROR_MESSAGE );

         }
       
    
        return null;
    }
    
    public String EliminarRegistrarPasaje(String ruta,String pasajero){
        String mensaje="",query;
        int resultado;
        query= "DELETE FROM registrar_pasaje  WHERE id_ruta='" + ruta + "' AND id_pasajero='"+pasajero+"';";
        
        
        try {
            Connection conexion = this.fachada.getConexion();
            Statement sentencia = conexion.createStatement();
            resultado = sentencia.executeUpdate(query);
            if (resultado == 1) {
                mensaje = "Registro eliminado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar la eliminación :" + ex.getMessage();
        }
    
        return mensaje;
    
    }
    
    public String ActualizarRegistrarPasaje(RegistrarPasaje registrarPasaje,String ruta,String pasajero){
        String mensaje="",query;
        int resultado;
        query= "UPDATE registrar_pasaje set (id_ruta,id_pasajero,fecha_hora) "
                + "= ('" +registrarPasaje.getRuta()+ "', '" +registrarPasaje.getPasajero()+"', '"+ registrarPasaje.getFecha()+"')"
                + " WHERE id_ruta='" + ruta + "' AND id_pasajero='"+pasajero+"';";
        
        
        try {
            Connection conexion = this.fachada.getConexion();
            Statement sentencia = conexion.createStatement();
            resultado = sentencia.executeUpdate(query);
            if (resultado == 1) {
                mensaje = "Registro Actualizado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar la actualización :" + ex.getMessage();
        }
    
        return mensaje;
    }
    
    
}
