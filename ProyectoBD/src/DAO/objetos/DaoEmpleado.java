
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.objetos;

import AccesoDatos.Fachada;
import DAO.logica.Empleado;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author flavio
 */


public class DaoEmpleado {

    Fachada fachada;

    public DaoEmpleado() {
        this.fachada = new Fachada();
    }

    public String IngresarEmpleado(Empleado empleado) {
        String mensaje = "", query;
        int resultado;
        query = "INSERT INTO empleados (cedula_empleado,nombre_empleado,telefono_empleado,direccion_empleado,email_empleado"
                + ",cargo_empleado,salario_empleado,id_estacion,estado_empleado,user_empleado,pass_empleado )VALUES ('"
                + empleado.getCedula() + "', '" + empleado.getNombre() + "', '" + empleado.getTelefono() + "', '"
                + empleado.getDireccion() + "', '" + empleado.getEmail() + "', '" + empleado.getCargo()
                + "', '" + empleado.getSalario() + "', '" + empleado.getIdEstacion() + "', '" + empleado.getEstado() + "',"
                + "'" + empleado.getUsuario() + "','" + empleado.getContrasenha() + "');";
        System.out.println(query);

        try {
            Connection conexion = this.fachada.getConexion();
            Statement sentencia = conexion.createStatement();
            resultado = sentencia.executeUpdate(query);
            if (resultado == 1) {
                mensaje = "Registro ingresado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar el registro :" + ex.getMessage();
        }

        return mensaje;
    }

    public Empleado ConsultarEmpleado(String identificacion) {

        Empleado empleado = new Empleado();
        String sql_select;
        sql_select = "SELECT id_empleado,cedula_empleado,nombre_empleado,telefono_empleado,direccion_empleado,email_empleado"
                + ",cargo_empleado,salario_empleado,id_estacion,estado_empleado,user_empleado,pass_empleado "
                + "FROM  empleados WHERE id_empleado='" + identificacion + "'";
        try {
            Connection conn = fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql_select);

            while (resultado.next()) {

                empleado.setId(resultado.getString(1));
                empleado.setCedula(resultado.getString(2));
                empleado.setNombre(resultado.getString(3));
                empleado.setTelefono(resultado.getString(4));
                empleado.setDireccion(resultado.getString(5));
                empleado.setEmail(resultado.getString(6));
                empleado.setCargo(resultado.getString(7));
                empleado.setSalario(resultado.getInt(8));
                empleado.setIdEstacion(resultado.getString(9));
                empleado.setEstado(resultado.getString(10));
                empleado.setUsuario(resultado.getString(11));
                empleado.setContrasenha(resultado.getString(12));

            }

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en empleado", JOptionPane.ERROR_MESSAGE);
            empleado = null;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error desconocido", "Error en empleado", JOptionPane.ERROR_MESSAGE);
            empleado = null;

        }

        return empleado;
    }   

    public Empleado ConsultarEmpleadoCedula(String identificacion) {

        Empleado empleado = new Empleado();
        String sql_select;
        sql_select = "SELECT id_empleado,cedula_empleado,nombre_empleado,telefono_empleado,direccion_empleado,email_empleado"
                + ",cargo_empleado,salario_empleado,id_estacion,estado_empleado,estado_empleado,user_empleado,pass_empleado "
                + "FROM  empleados WHERE cedula_empleado='" + identificacion + "'";
        try {
            Connection conn = fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql_select);

            while (resultado.next()) {

                empleado.setId(resultado.getString(1));
                empleado.setCedula(resultado.getString(2));
                empleado.setNombre(resultado.getString(3));
                empleado.setTelefono(resultado.getString(4));
                empleado.setDireccion(resultado.getString(5));
                empleado.setEmail(resultado.getString(6));
                empleado.setCargo(resultado.getString(7));
                empleado.setSalario(resultado.getInt(8));
                empleado.setIdEstacion(resultado.getString(9));
                empleado.setEstado(resultado.getString(10));
                empleado.setUsuario(resultado.getString(11));
                empleado.setContrasenha(resultado.getString(12));
            }

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en empleado", JOptionPane.ERROR_MESSAGE);
            empleado = null;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error desconocido", "Error en empleado", JOptionPane.ERROR_MESSAGE);
            empleado = null;

        }

        return empleado;
    }

    public Empleado ConsultarEmpleadoUser(String identificacion) {

        Empleado empleado = new Empleado();
        String sql_select;
        sql_select = "SELECT id_empleado,cedula_empleado,nombre_empleado,telefono_empleado,direccion_empleado,email_empleado"
                + ",cargo_empleado,salario_empleado,id_estacion,estado_empleado,user_empleado,pass_empleado "
                + "FROM  empleados WHERE user_empleado='" + identificacion + "'";
        try {
            Connection conn = fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql_select);

            while (resultado.next()) {

                empleado.setId(resultado.getString(1));
                empleado.setCedula(resultado.getString(2));
                empleado.setNombre(resultado.getString(3));
                empleado.setTelefono(resultado.getString(4));
                empleado.setDireccion(resultado.getString(5));
                empleado.setEmail(resultado.getString(6));
                empleado.setCargo(resultado.getString(7));
                empleado.setSalario(resultado.getInt(8));
                empleado.setIdEstacion(resultado.getString(9));
                empleado.setEstado(resultado.getString(10));
                empleado.setUsuario(resultado.getString(11));
                empleado.setContrasenha(resultado.getString(12));
            }

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en empleado", JOptionPane.ERROR_MESSAGE);
            empleado = null;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error desconocido", "Error en empleado", JOptionPane.ERROR_MESSAGE);
            empleado = null;

        }

        return empleado;
    }

    public ResultSet TreaerConductores() {

        String sql_select;
        sql_select = "SELECT id_empleado,cedula_empleado,nombre_empleado,telefono_empleado,direccion_empleado,email_empleado"
                + ",cargo_empleado,salario_empleado,id_estacion,estado_empleado "
                + "FROM  empleados WHERE cargo_empleado='CONDUCTOR' ORDER BY nombre_empleado";
        ResultSet resultado = null;
        try {
            Connection conn = fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            resultado = sentencia.executeQuery(sql_select);

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en empleado", JOptionPane.ERROR_MESSAGE);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error desconocido", "Error en empleado", JOptionPane.ERROR_MESSAGE);

        }

        return resultado;

    }

    public String EliminarEmpleado(String identificacion) {
        String mensaje = "", query;
        int resultado;
        query = "DELETE FROM empleados WHERE id_empleado ='" + identificacion + "';";

        try {
            Connection conexion = this.fachada.getConexion();
            Statement sentencia = conexion.createStatement();
            resultado = sentencia.executeUpdate(query);
            if (resultado == 1) {
                mensaje = "Registro eliminado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar la eliminación :" + ex.getMessage();
        }

        return mensaje;

    }

    public String ActualizarEmpleado(Empleado empleado) {
        String mensaje = "", query;
        int resultado;
        query = "UPDATE empleados set(cedula_empleado,nombre_empleado,telefono_empleado,direccion_empleado,email_empleado"
                + ",cargo_empleado,salario_empleado,id_estacion,estado_empleado )=('"
                + empleado.getCedula() + "', '" + empleado.getNombre() + "', '" + empleado.getTelefono() + "', '"
                + empleado.getDireccion() + "', '" + empleado.getEmail() + "', '" + empleado.getCargo()
                + "', '" + empleado.getSalario() + "', '" + empleado.getIdEstacion() + "', '" + empleado.getEstado() + "')"
                + " WHERE id_empleado ='" + empleado.getId() + "';";
        System.out.println(query + "\n");

        try {
            Connection conexion = this.fachada.getConexion();
            Statement sentencia = conexion.createStatement();
            resultado = sentencia.executeUpdate(query);
            if (resultado == 1) {
                mensaje = "Registro Actualizado con exito";
            } else {

            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar la actualización :" + ex.getMessage();
        }

        return mensaje;
    }

    public ResultSet TreaerJefeEstacion(String estacion) {

        String sql_select;
        sql_select = "SELECT id_empleado,cedula_empleado,nombre_empleado,telefono_empleado,direccion_empleado,email_empleado"
                + ",cargo_empleado,salario_empleado,id_estacion,estado_empleado "
                + "FROM  empleados WHERE cargo_empleado='AUXILIAR'  AND id_estacion = '" + estacion + "' ORDER BY nombre_empleado";
        ResultSet resultado = null;
        try {
            Connection conn = fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            resultado = sentencia.executeQuery(sql_select);

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en empleado", JOptionPane.ERROR_MESSAGE);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error desconocido", "Error en empleado", JOptionPane.ERROR_MESSAGE);

        }

        return resultado;
    }

}
