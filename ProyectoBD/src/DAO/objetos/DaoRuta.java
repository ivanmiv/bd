/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.objetos;

import AccesoDatos.Fachada;
import DAO.logica.Ruta;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author flavio
 */
public class DaoRuta {

    Fachada fachada;

    public DaoRuta() {
        this.fachada = new Fachada();
    }

    public String IngresarRuta(Ruta ruta) {
        String mensaje = "", query;
        int resultado;
        query = "INSERT INTO rutas (nombre_ruta,descripcion_recorrido )"
                + "VALUES ('" + ruta.getNombre() + "', '" + ruta.getDescripcion() + "');";

        try {
            Connection conexion = this.fachada.getConexion();
            Statement sentencia = conexion.createStatement();
            resultado = sentencia.executeUpdate(query);
            if (resultado == 1) {
                mensaje = "Registro ingresado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar el registro :" + ex.getMessage();
        }

        return mensaje;
    }

    public Ruta ConsultarRuta(String identificacion) {

        Ruta ruta = new Ruta();
        String sql_select;
        sql_select = "SELECT id_ruta,nombre_ruta,descripcion_recorrido  "
                + "FROM  rutas WHERE id_ruta='" + identificacion + "'";
        try {
            Connection conn = fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql_select);

            while (resultado.next()) {

                ruta.setId(resultado.getString(1));
                ruta.setNombre(resultado.getString(2));
                ruta.setDesscripcion(resultado.getString(3));

            }

            return ruta;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en ruta", JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error desconocido", "Error en ruta", JOptionPane.ERROR_MESSAGE);

        }

        return null;
    }
    
    public Ruta ConsultarRutaNombre(String identificacion) {

        Ruta ruta = new Ruta();
        String sql_select;
        sql_select = "SELECT id_ruta,nombre_ruta,descripcion_recorrido  "
                + "FROM  rutas WHERE nombre_ruta='" + identificacion + "'";
        try {
            Connection conn = fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql_select);

            while (resultado.next()) {

                ruta.setId(resultado.getString(1));
                ruta.setNombre(resultado.getString(2));
                ruta.setDesscripcion(resultado.getString(3));

            }

            return ruta;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en ruta", JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error desconocido", "Error en ruta", JOptionPane.ERROR_MESSAGE);

        }

        return null;
    }

    public String EliminarRuta(String identificacion) {
        String mensaje = "", query;
        int resultado;
        query = "DELETE FROM rutas WHERE id_ruta='" + identificacion + "';";

        try {
            Connection conexion = this.fachada.getConexion();
            Statement sentencia = conexion.createStatement();
            resultado = sentencia.executeUpdate(query);
            if (resultado == 1) {
                mensaje = "Registro eliminado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar la eliminación :" + ex.getMessage();
        }

        return mensaje;

    }

    public String ActualizarRuta(Ruta ruta) {
        String mensaje = "", query;
        int resultado;
        query = "UPDATE rutas set (nombre_ruta,descripcion_recorrido) "
                + "= ('" + ruta.getNombre() + "','" + ruta.getDescripcion() + "')"
                + " WHERE id_ruta ='" + ruta.getId() + "';";

        try {
            Connection conexion = this.fachada.getConexion();
            Statement sentencia = conexion.createStatement();
            resultado = sentencia.executeUpdate(query);
            if (resultado == 1) {
                mensaje = "Registro Actualizado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar la actualización :" + ex.getMessage();
        }

        return mensaje;
    }

    public ArrayList<Ruta> ConsultarRutas() {
        ArrayList<Ruta> rutas = new ArrayList<>();
        Ruta ruta = null;
        String sql_select;
        sql_select = "SELECT id_ruta,nombre_ruta,descripcion_recorrido  "
                + "FROM  rutas;";
        try {
            Connection conn = fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql_select);

            while (resultado.next()) {
                ruta = new Ruta();
                ruta.setId(resultado.getString(1));
                ruta.setNombre(resultado.getString(2));
                ruta.setDesscripcion(resultado.getString(3));
                rutas.add(ruta);
            }

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en ruta", JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error desconocido", "Error en ruta", JOptionPane.ERROR_MESSAGE);

        }

        return rutas;
    }

    public ResultSet consultarNombreDescripcionRuta() {
        String sql_select;
        sql_select = "SELECT nombre_ruta,descripcion_recorrido  "
                + "FROM  rutas;";
        try {
            Connection conn = fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql_select);

            return resultado;

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en ruta", JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error desconocido", "Error en ruta", JOptionPane.ERROR_MESSAGE);

        }

        return null;
    }

    public String consultarParadas(String identificacion) {
        String paradas = "";
        String sql_select;
        sql_select = "SELECT nombre_estacion   "
                + " FROM atender INNER JOIN estaciones ON atender.id_estacion = estaciones.id_estacion   "
                + " WHERE id_ruta='" + identificacion + "'";
        try {
            Connection conn = fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql_select);

            while (resultado.next()) {
                paradas += resultado.getString("nombre_estacion") + "\n";
            }
            return paradas;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en ruta", JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error desconocido", "Error en ruta", JOptionPane.ERROR_MESSAGE);

        }

        return null;
    }

    public ArrayList<Ruta> consultarEstacionesFromTo(String id_estacionFrom, String id_estacionTo) {
        Ruta ruta = null;
        ArrayList<Ruta> rutasQueMeSirven = new ArrayList<Ruta>();
        String paradas = "";
        String sql_select;
        
        sql_select = "SELECT R.nombre_ruta,R.descripcion_recorrido FROM\n"
                + "(SELECT rutas.id_ruta FROM rutas INNER JOIN atender ON atender.id_ruta = rutas.id_ruta  "
                + "WHERE atender.id_estacion = '"+id_estacionFrom+"'\n"
                + "AND rutas.id_ruta IN (SELECT rutas.id_ruta FROM rutas INNER JOIN atender "
                + "ON atender.id_ruta = rutas.id_ruta  WHERE atender.id_estacion = '"+id_estacionTo+"'))AS rutas\n"
                + "INNER JOIN rutas R ON R.id_ruta = rutas.id_ruta;";
        System.out.println("consultaaaaaaa es \n"+sql_select+"\n");
        try {
            Connection conn = fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql_select);

            while (resultado.next()) {
                ruta = new Ruta();
                
                ruta.setNombre(resultado.getString(1));
                
                ruta.setDesscripcion(resultado.getString(2));
                rutasQueMeSirven.add(ruta);System.out.println("rruuuuuuuuuuta: "+ruta.getNombre()+"\n");
            }
            //return rutasQueMeSirven;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en ruta", JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
            
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Error desconocido", "Error en ruta", JOptionPane.ERROR_MESSAGE);

        }

        return rutasQueMeSirven;
    }
}
