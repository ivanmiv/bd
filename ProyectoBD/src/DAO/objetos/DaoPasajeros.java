/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.objetos;

import AccesoDatos.Fachada;
import DAO.logica.Pasajero;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author flavio
 */
public class DaoPasajeros {

    Fachada fachada;

    public DaoPasajeros() {
        this.fachada = new Fachada();
    }

    public String IngresarPasajero(Pasajero pasajero) {
        String mensaje = "", query;
        int resultado;
        query = "INSERT INTO pasajeros (cedula_pasajero,nombre_pasajero,telefono_pasajero,pin_tarjeta)"
                + " VALUES ('" + pasajero.getCedula() + "', '" + pasajero.getNombre() + "', '" + pasajero.getTelefono()
                + "', '" + pasajero.getTarjeta() + "');";

        try {
            Connection conexion = this.fachada.getConexion();
            Statement sentencia = conexion.createStatement();
            resultado = sentencia.executeUpdate(query);
            if (resultado == 1) {
                mensaje = "Registro ingresado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar el registro :" + ex.getMessage();
        }

        return mensaje;
    }

    public Pasajero ConsultarPasajero(String identificacion) {

        Pasajero pasajero = new Pasajero();
        String sql_select;
        sql_select = "SELECT id_pasajero,cedula_pasajero,nombre_pasajero,pin_tarjeta"
                + "FROM  pasajeros WHERE id_pasajero='" + identificacion + "'";
        try {
            Connection conn = fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql_select);

            while (resultado.next()) {

                pasajero.setId(resultado.getString(1));
                pasajero.setCedula(resultado.getString(2));
                pasajero.setNombre(resultado.getString(3));
                pasajero.setTelefono(resultado.getString(4));
                pasajero.setTarjeta(resultado.getString(5));

            }

            return pasajero;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en pasajero", JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
            pasajero.setId("NOEXISTE");
            return pasajero;

        }

        return null;
    }

    public String EliminarPasajero(String identificacion) {
        String mensaje = "", query;
        int resultado;
        query = "DELETE FROM pasajeros WHERE id_pasajero ='" + identificacion + "';";

        try {
            Connection conexion = this.fachada.getConexion();
            Statement sentencia = conexion.createStatement();
            resultado = sentencia.executeUpdate(query);
            if (resultado == 1) {
                mensaje = "Registro eliminado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar la eliminación :" + ex.getMessage();
        }

        return mensaje;

    }

    public String ActualizarPasajero(Pasajero pasajero) {
        String mensaje = "", query;
        int resultado;
        query = "UPDATE pasajeros set(cedula_pasajero,nombre_pasajero,telefono_pasajero,pin_tarjeta )=('"
                + pasajero.getCedula() + "', '" + pasajero.getNombre() + "', '" + pasajero.getTelefono()
                + "','" + pasajero.getTarjeta() + "')"
                + " WHERE id_pasajero ='" + pasajero.getId() + "';";

        try {
            Connection conexion = this.fachada.getConexion();
            Statement sentencia = conexion.createStatement();
            resultado = sentencia.executeUpdate(query);
            if (resultado == 1) {
                mensaje = "Registro Actualizado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar la actualización :" + ex.getMessage();
        }

        return mensaje;
    }

    public int comprobarTarjetaNoRegistradaOtroUsuario(String pin) {
        int siOno = 0;
        String sql_select;
        sql_select = "SELECT COUNT(*) FROM pasajeros WHERE pin_tarjeta='" + pin + "'";
        try {
            Connection conn = fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql_select);

            while (resultado.next()) {

                siOno = Integer.parseInt(resultado.getString(1));

            }

            return siOno;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en tarjeta", JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error desconocido", "Error en tarjeta", JOptionPane.ERROR_MESSAGE);

        }
        return 0;
    }

    public Pasajero consultarPasajeroPorTarjeta(String pin_tarjeta) {
        System.out.println(pin_tarjeta + "\n");
        Pasajero pasajero_consultado = new Pasajero();
        String sql_select;
        sql_select = "SELECT id_pasajero,cedula_pasajero,nombre_pasajero,telefono_pasajero,pin_tarjeta\"\n"
                + "                + \"FROM  pasajeros WHERE pin_tarjeta='" + pin_tarjeta + "'";
        try {
            try {
                Connection conn = fachada.getConexion();
                System.out.println("consultando en la bd");
                Statement sentencia = conn.createStatement();
                ResultSet resultado = sentencia.executeQuery(sql_select);

                while (resultado.next()) {

                    pasajero_consultado.setId(resultado.getString(1));
                    pasajero_consultado.setCedula(resultado.getString(2));
                    pasajero_consultado.setNombre(resultado.getString(3));
                    pasajero_consultado.setTelefono(resultado.getString(4));
                    pasajero_consultado.setTarjeta(resultado.getString(5));

                }

                return pasajero_consultado;

            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en tarjeta", JOptionPane.ERROR_MESSAGE);
            } catch (NullPointerException e) {
                pasajero_consultado.setId("NOEXISTE");
                return pasajero_consultado;

            }
        } catch (NullPointerException e) {
            pasajero_consultado.setId("NOEXISTE");
            return pasajero_consultado;

        }
        return pasajero_consultado;
    }

    public Pasajero ConsultarPasajeroCedula(String cedula) {
        Pasajero pasajero = new Pasajero();
        String sql_select;
        sql_select = "SELECT id_pasajero,cedula_pasajero,nombre_pasajero,telefono_pasajero,pin_tarjeta"
                + " FROM  pasajeros WHERE cedula_pasajero='" + cedula + "';";

        System.out.println(sql_select);
        try {
            Connection conn = fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql_select);

            while (resultado.next()) {

                pasajero.setId(resultado.getString(1));
                pasajero.setCedula(resultado.getString(2));
                pasajero.setNombre(resultado.getString(3));
                pasajero.setTelefono(resultado.getString(4));
                pasajero.setTarjeta(resultado.getString(5));

            }

            return pasajero;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en pasajero", JOptionPane.ERROR_MESSAGE);
            pasajero.setId("NOEXISTE");

            return pasajero;
        } catch (Exception e) {
            pasajero.setId("NOEXISTE");
            return pasajero;

        }

    }

    public String rutasFrecuentes(String id_pasajero) {
        String mensaje = "NO DISPONIBLE";
        String sql_select = "SELECT R.id_ruta, R.count,rutas.nombre_ruta FROM\n"
                + "\n"
                + "(SELECT id_ruta,count(id_ruta) FROM registrar_pasaje  WHERE id_pasajero = '"+id_pasajero+"' "
                + "GROUP BY id_ruta HAVING count(id_ruta)>=1)AS R "
                + "INNER JOIN rutas ON R.id_ruta = rutas.id_ruta;";
        
        try {
            Connection conn = fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql_select);

            while (resultado.next()) {

                mensaje += resultado.getString(3) + " " + resultado.getString(2) + " veces\n";

            }

            
        } catch (SQLException e) {
            
            return mensaje;
        } catch (Exception e) {
            
            return mensaje;

        }

        
        
        return mensaje;
    }
}
