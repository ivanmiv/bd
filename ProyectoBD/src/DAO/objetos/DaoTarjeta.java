/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.objetos;

import AccesoDatos.Fachada;
import DAO.logica.Tarjeta;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author flavio
 */
public class DaoTarjeta {
    Fachada fachada;

    public DaoTarjeta() {
        this.fachada =  new Fachada();
    }
    
    
    public String IngresarTarjeta(Tarjeta tarjeta){
        String mensaje="",query;
        int resultado;
        query= "INSERT INTO tarjetas (saldo_tarjeta,estado_tarjeta,estacion_id,fecha_venta)"
                + " VALUES ('" +tarjeta.getSaldo()+ "', '" + tarjeta.getEstado()+"', '"+tarjeta.getEstacion_id_creacion()+"',now());";
        
        
        try {
            Connection conexion = this.fachada.getConexion();
            Statement sentencia = conexion.createStatement();
            resultado = sentencia.executeUpdate(query);
            if (resultado == 1) {
                mensaje = "Registro ingresado con exito.";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar el registro :" + ex.getMessage();
        }
    
        return mensaje;
    }
    
    public String obtenerMaximoRegistroTarjeta(){
        String mensaje="",sql_select;
        sql_select= "SELECT count(pin_tarjeta) FROM tarjetas;";
        
        
        try {
            Connection conexion = this.fachada.getConexion();
            Statement sentencia = conexion.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql_select);
            
            while(resultado.next()){
                
               mensaje=resultado.getString(1);
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar la consulta por :" + ex.getMessage();
        }
    
        return mensaje;
    }
    
    public Tarjeta ConsultarTarjeta(String identificacion){
        
        Tarjeta tarjeta= new Tarjeta();
        String sql_select;
        sql_select="SELECT pin_tarjeta,saldo_tarjeta,estado_tarjeta "
                + "FROM  tarjetas WHERE pin_tarjeta='" + identificacion +  "'";
         try{
            Connection conn= fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql_select);
            
            while(resultado.next()){
                
               tarjeta.setPin(resultado.getString(1));
               tarjeta.setSaldo(resultado.getInt(2));
               tarjeta.setEstado(resultado.getString(3));
              
            }
            int pin = Integer.parseInt(tarjeta.getPin());
           
            return tarjeta;
         }
         catch(SQLException e){ 
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en tarjeta",JOptionPane.ERROR_MESSAGE );
         }catch(NumberFormatException e){
             tarjeta.setPin("NOEXISTE");
         }
         
         catch(Exception e){
            JOptionPane.showMessageDialog(null, "Error desconocido", "Error en tarjeta",JOptionPane.ERROR_MESSAGE );

         }
       
    
        return tarjeta;
    }
    
    public String EliminarTarjeta(String identificacion){
        String mensaje="",query;
        int resultado;
        query= "DELETE FROM tarjetas WHERE pin_tarjeta='" + identificacion +  "';";
        
        
        try {
            Connection conexion = this.fachada.getConexion();
            Statement sentencia = conexion.createStatement();
            resultado = sentencia.executeUpdate(query);
            if (resultado == 1) {
                mensaje = "Registro eliminado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar la eliminación :" + ex.getMessage();
        }
    
        return mensaje;
    
    }
    
    public String ActualizarTarjeta(Tarjeta tarjeta){
        String mensaje="",query;
        int resultado;
        query= "UPDATE tarjetas set (saldo_tarjeta,estado_tarjeta ) "
                + "= ( " +tarjeta.getSaldo()+",'"+tarjeta.getEstado()+"')"
                + " WHERE pin_tarjeta ='" + tarjeta.getPin()+"';";
        
        
        try {
            Connection conexion = this.fachada.getConexion();
            Statement sentencia = conexion.createStatement();
            resultado = sentencia.executeUpdate(query);
            if (resultado == 1) {
                mensaje = "Registro Actualizado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar la actualización :" + ex.getMessage();
        }
    
        return mensaje;
    }

    
    
}
