/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.objetos;

import AccesoDatos.Fachada;
import DAO.logica.Solicitud;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author dacer
 */
public class DaoSolicitud {

    Fachada fachada;

    public DaoSolicitud() {
        this.fachada = new Fachada();
    }

    public String IngresarSolicitud(Solicitud solicitud) {
        String mensaje = "", query;
        int resultado;
        query = "INSERT INTO solicitudPQRS (motivo_solicitud,descripcion_solicitud,fecha_solicitud,"
                + "estado_solicitud,pasajero_id,estacion_id)"
                + " VALUES ('" + solicitud.getMotivo() + "', '" + solicitud.getIdescripcion() + "', '"
                + solicitud.getFechaSolicitud() + "', '" + solicitud.getEstado() + "', '"
                + solicitud.getPasajero() + "', '" + solicitud.getEstacion() + "');";

        try {
            Connection conexion = this.fachada.getConexion();
            Statement sentencia = conexion.createStatement();
            resultado = sentencia.executeUpdate(query);

            System.out.println(resultado);
            if (resultado == 1) {
                mensaje = "Registro ingresado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar el registro :" + ex.getMessage();
        }

        return mensaje;
    }

    public Solicitud ConsultarSolicitud(String identificacion) {

        Solicitud solicitud = new Solicitud();
        String sql_select;
        sql_select = "SELECT id_solicitud,motivo_solicitud,descripcion_solicitud,fecha_solicitud,"
                + "estado_solicitud,pasajero_id,estacion_id "
                + "FROM  solicitudPQRS WHERE id_solicitud='" + identificacion + "';";
        System.out.println(sql_select);
        try {
            Connection conn = fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql_select);
            //String sql_select2 = "SELECT * FROM  accionesPQRS WHERE id_solicitud = '" + identificacion + "';";
            //ResultSet resultado2 = sentencia.executeQuery(sql_select2);

            while (resultado.next()) {

                solicitud.setId(resultado.getString(1));

                solicitud.setMotivo(resultado.getString(2));
                solicitud.setIdescripcion(resultado.getString(3));
                solicitud.setFechaSolicitud(resultado.getString(4));
                solicitud.setEstado(resultado.getString(5));

                solicitud.setPasajero(resultado.getString(6));
                solicitud.setEstacion(resultado.getString(7));

            }

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en solicitud", JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error desconocido", "Error en solicitud", JOptionPane.ERROR_MESSAGE);
        }
        
        ArrayList<String> acciones = new ArrayList<>();
        try {
            Connection conn = fachada.getConexion();
            Statement sentencia = conn.createStatement();
            String sql_select2 = "SELECT * FROM  accionesPQRS WHERE id_solicitud = '" + identificacion + "';";
            ResultSet resultado2 = sentencia.executeQuery(sql_select2);
            while (resultado2.next()) {
                acciones.add(resultado2.getString(2));

            }
            solicitud.addAcciones(acciones);
            
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en solicitud", JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error desconocido", "Error en solicitud", JOptionPane.ERROR_MESSAGE);

        }
        

        return solicitud;

    }

    public String EliminarSolicitud(String identificacion) {
        String mensaje = "", query;
        int resultado;
        query = "DELETE FROM solicitudPQRS WHERE id_solicitud ='" + identificacion + "';";

        try {
            Connection conexion = this.fachada.getConexion();
            Statement sentencia = conexion.createStatement();
            resultado = sentencia.executeUpdate(query);
            if (resultado == 1) {
                mensaje = "Registro eliminado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar la eliminación :" + ex.getMessage();
        }

        return mensaje;

    }

    public String ActualizarSolicitud(Solicitud solicitud) {
        String mensaje = "", query;
        int resultado;
        query = "UPDATE solicitudPQRS set(motivo_solicitud,descripcion_solicitud,fecha_solicitud,"
                + "estado_solicitud,pasajero_id,estacion_id)=('" + solicitud.getMotivo() + "', '" + solicitud.getIdescripcion() + "', '"
                + solicitud.getFechaSolicitud() + "', '" + solicitud.getEstado() + "', '"
                + solicitud.getPasajero() + "', '" + solicitud.getEstacion() + "')"
                + " WHERE id_solicitud ='" + solicitud.getId() + "';";

        try {
            Connection conexion = this.fachada.getConexion();
            Statement sentencia = conexion.createStatement();
            resultado = sentencia.executeUpdate(query);
            if (resultado == 1) {
                mensaje = "Registro Actualizado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar la actualización :" + ex.getMessage();
        }

        return mensaje;
    }

    public String traerMaximoSolicitudes() {
        String max = null;
        try {
            Connection conn = fachada.getConexion();
            Statement sentencia = conn.createStatement();
            String sql_select2 = "SELECT MAX(id_solicitud) FROM  solicitudPQRS;";
            ResultSet resultado2 = sentencia.executeQuery(sql_select2);
            while (resultado2.next()) {
                max = (resultado2.getString(1));

            }

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en solicitud", JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error desconocido", "Error en solicitud", JOptionPane.ERROR_MESSAGE);

        }
        return max;
    }

    public String adicionarAccion(String ticket, String accion) {
        String mensaje = "", query;
        int resultado;
        query = "INSERT INTO accionesPQRS "
                + " VALUES ('" + ticket + "','" + accion + "');";

        try {
            Connection conexion = this.fachada.getConexion();
            Statement sentencia = conexion.createStatement();
            resultado = sentencia.executeUpdate(query);
            if (resultado == 1) {
                mensaje = "Registro ingresado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar el registro :" + ex.getMessage();
        }

        return mensaje;
    }

    public String TraerSolicitudReversa(String motivo, String fecha, String pasajero) {
        String sql_select;

        sql_select = "SELECT id_solicitud "
                + "FROM  solicitudPQRS WHERE motivo_solicitud='" + motivo + "' AND fecha_solicitud ='" + fecha + "'"
                + " AND  pasajero_id = '" + pasajero + "';";
        System.out.println(sql_select);
        String resul = "";
        try {

            Connection conn = fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql_select);

            while (resultado.next()) {

                return resultado.getString(1);

            }

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en solicitud", JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error desconocido", "Error en solicitud", JOptionPane.ERROR_MESSAGE);
        }

        return resul;

    }

    public ResultSet consultarSolicitudesComunes() {
        String query;

        query = "SELECT UPPER(motivo_solicitud) motivo , COUNT(*) cantidad \n"
                + " FROM solicitudPQRS \n"
                + " GROUP BY UPPER(motivo_solicitud)\n"
                + " ORDER BY cantidad desc\n"
                + " LIMIT 5";
        System.out.println(query);
        ResultSet resultado = null;
        try {

            Connection conn = fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            resultado = sentencia.executeQuery(query);

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en solicitud", JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error desconocido", "Error en solicitud", JOptionPane.ERROR_MESSAGE);
        }

        return resultado;
    }

}
