/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.objetos;

import AccesoDatos.Fachada;
import DAO.logica.Atender;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author dacer
 */
public class DaoAtender {
    
    Fachada fachada;

    public DaoAtender() {
        this.fachada =  new Fachada();
        
    }
    
    
    public String IngresarAtender(Atender atender){
        String mensaje="",query;
        int resultado;
        query= "INSERT INTO atender (id_ruta,id_estacion )"
                + "VALUES ('" +atender.getIdRuta()+ "', '" + atender.getIdEstacion()+"');";
        
        
        try {
            Connection conexion = this.fachada.getConexion();
            Statement sentencia = conexion.createStatement();
            resultado = sentencia.executeUpdate(query);
            if (resultado == 1) {
                mensaje = "Registro ingresado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar el registro :" + ex.getMessage();
        }
    
        return mensaje;
    }
    
    public Atender ConsultarAtender(String identificacionRuta , String identificacionEstacion){
        
        Atender atender= new Atender();
        String sql_select;
        sql_select="SELECT id_ruta,id_estacion "
                + "FROM  atender WHERE id_ruta='" + identificacionRuta +  "' AND "
                + "id_estacion = '"+identificacionEstacion+"' ;";
         try{
            Connection conn= fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql_select);
            
            while(resultado.next()){
                
               atender.setIdRuta(resultado.getString(1));
               atender.setIdEstacion(resultado.getString(2));
              
            }
           
            return atender;
         }
         catch(SQLException e){ 
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en atender",JOptionPane.ERROR_MESSAGE );
         }
         catch(Exception e){
            JOptionPane.showMessageDialog(null, "Error desconocido", "Error en atender",JOptionPane.ERROR_MESSAGE );

         }
       
    
        return null;
    }
    
    public String EliminarAtender(Atender atender){
        String mensaje="",query;
        int resultado;
        query= "DELETE FROM atender WHERE id_ruta='" + atender.getIdRuta() +  "' AND "
                + "id_estacion = '"+atender.getIdEstacion()+"' ;";
        
        
        try {
            Connection conexion = this.fachada.getConexion();
            Statement sentencia = conexion.createStatement();
            resultado = sentencia.executeUpdate(query);
            if (resultado == 1) {
                mensaje = "Registro eliminado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar la eliminación :" + ex.getMessage();
        }
    
        return mensaje;
    
    }
    
    public String ActualizarAtender(Atender atender, String idruta,String idestacion){
        String mensaje="",query;
        int resultado;
        query= "UPDATE atenders set (id_ruta,id_estacion  ) "
                + "= ('" +atender.getIdRuta()+"','"+atender.getIdEstacion()+"')"
                + " WHERE id_ruta ='" + idruta+"' AND id_estacion ='"+idestacion+"' ;";
        
        
        try {
            Connection conexion = this.fachada.getConexion();
            Statement sentencia = conexion.createStatement();
            resultado = sentencia.executeUpdate(query);
            if (resultado == 1) {
                mensaje = "Registro Actualizado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar la actualización :" + ex.getMessage();
        }
    
        return mensaje;
    }

   
    
}
