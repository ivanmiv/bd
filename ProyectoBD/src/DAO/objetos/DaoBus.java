/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.objetos;

import AccesoDatos.Fachada;
import DAO.logica.Bus;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author dacer
 */
public class DaoBus {

    Fachada fachada;

    public DaoBus() {
        this.fachada = new Fachada();
    }

    public String IngresarBus(Bus bus) {
        String mensaje = "", query;
        int resultado;
        query = "INSERT INTO buses (numero_interno_bus,placa_bus,tipo_bus,id_ruta )"
                + "VALUES ('" + bus.getNumero() + "', '" + bus.getPlaca() + "', '" + bus.getTipo() + "', '" + bus.getRuta() + "');";

        try {
            Connection conexion = this.fachada.getConexion();
            Statement sentencia = conexion.createStatement();
            resultado = sentencia.executeUpdate(query);
            if (resultado == 1) {
                mensaje = "Registro ingresado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar el registro :" + ex.getMessage();
        }

        return mensaje;
    }

    public Bus ConsultarBus(String idbus) {

        Bus bus = new Bus();
        String sql_select;
        sql_select = "SELECT id_bus,numero_interno_bus,placa_bus,tipo_bus,id_ruta "
                + "FROM  buses WHERE id_bus='" + idbus + "';";
        try {
            Connection conn = fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql_select);

            while (resultado.next()) {

                bus.setId(resultado.getString(1));
                bus.setNumero(resultado.getString(2));
                bus.setPlaca(resultado.getString(3));
                bus.setTipo(resultado.getString(4));
                bus.setRuta(resultado.getString(5));

            }

            return bus;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en bus", JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error desconocido", "Error en bus", JOptionPane.ERROR_MESSAGE);

        }

        return null;
    }

    public Bus ConsultarBusNUmeroInterno(String idbus) {

        Bus bus = new Bus();
        String sql_select;
        sql_select = "SELECT id_bus,numero_interno_bus,placa_bus,tipo_bus,id_ruta "
                + "FROM  buses WHERE numero_interno_bus='" + idbus + "';";
        try {
            Connection conn = fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql_select);

            while (resultado.next()) {

                bus.setId(resultado.getString(1));
                bus.setNumero(resultado.getString(2));
                bus.setPlaca(resultado.getString(3));
                bus.setTipo(resultado.getString(4));
                bus.setRuta(resultado.getString(5));

            }

            return bus;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en bus", JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error desconocido", "Error en bus", JOptionPane.ERROR_MESSAGE);

        }

        return null;
    }

    public String EliminarBus(String idbus) {
        String mensaje = "", query;
        int resultado;
        query = "DELETE FROM buses  WHERE id_bus='" + idbus + "';";

        try {
            Connection conexion = this.fachada.getConexion();
            Statement sentencia = conexion.createStatement();
            resultado = sentencia.executeUpdate(query);
            if (resultado == 1) {
                mensaje = "Registro eliminado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar la eliminación :" + ex.getMessage();
        }

        return mensaje;

    }

    public String ActualizarBus(Bus bus) {
        String mensaje = "", query;
        int resultado;
        query = "UPDATE buss set (numero_interno_bus,placa_bus,tipo_bus,id_ruta ) "
                + "= ('" + bus.getNumero() + "', '" + bus.getPlaca() + "', '" + bus.getTipo() + "', '" + bus.getRuta() + "')"
                + " WHERE id_bus='" + bus.getId() + "';";

        try {
            Connection conexion = this.fachada.getConexion();
            Statement sentencia = conexion.createStatement();
            resultado = sentencia.executeUpdate(query);
            if (resultado == 1) {
                mensaje = "Registro Actualizado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar la actualización :" + ex.getMessage();
        }

        return mensaje;
    }

    public ResultSet TreaerBuses() {

        String sql_select;
        sql_select = "SELECT id_bus,numero_interno_bus,placa_bus,tipo_bus,id_ruta,estado_bus"
                + " FROM  buses  ORDER BY numero_interno_bus";
        ResultSet resultado = null;
        try {
            Connection conn = fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            resultado = sentencia.executeQuery(sql_select);

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en buses", JOptionPane.ERROR_MESSAGE);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error desconocido", "Error en empleado", JOptionPane.ERROR_MESSAGE);

        }

        return resultado;

    }

    public ArrayList<Bus> consultaBusesTipologia(String tipologia) {
        ArrayList<Bus> buses = new ArrayList<>();

        String sql_select;
        sql_select = "SELECT id_bus,numero_interno_bus,placa_bus,tipo_bus,id_ruta "
                + "FROM  buses WHERE tipo_bus LIKE '" + tipologia + "';";
        try {
            Connection conn = fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql_select);

            while (resultado.next()) {
                Bus bus = new Bus();
                bus.setId(resultado.getString(1));
                bus.setNumero(resultado.getString(2));
                bus.setPlaca(resultado.getString(3));
                bus.setTipo(resultado.getString(4));
                bus.setRuta(resultado.getString(5));
                buses.add(bus);

            }

            return buses;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en bus", JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error desconocido", "Error en bus", JOptionPane.ERROR_MESSAGE);

        }

        return buses;
    }

    public Bus consultarBusPlaca(String placa) {
        Bus bus = new Bus();
        String sql_select;
        sql_select = "SELECT id_bus,numero_interno_bus,placa_bus,tipo_bus,id_ruta "
                + "FROM  buses WHERE placa_bus='" + placa + "';";
        try {
            Connection conn = fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql_select);

            while (resultado.next()) {

                bus.setId(resultado.getString(1));
                bus.setNumero(resultado.getString(2));
                bus.setPlaca(resultado.getString(3));
                bus.setTipo(resultado.getString(4));
                bus.setRuta(resultado.getString(5));

            }

            return bus;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en bus", JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error desconocido", "Error en bus", JOptionPane.ERROR_MESSAGE);

        }

        return null;
    }

    public Bus consultaBusNumeroInterno(String numeroInterno) {
        Bus bus = new Bus();
        String sql_select;
        sql_select = "SELECT id_bus,numero_interno_bus,placa_bus,tipo_bus,id_ruta "
                + "FROM  buses WHERE numero_interno_bus='" + numeroInterno + "';";
        try {
            Connection conn = fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql_select);

            while (resultado.next()) {

                bus.setId(resultado.getString(1));
                bus.setNumero(resultado.getString(2));
                bus.setPlaca(resultado.getString(3));
                bus.setTipo(resultado.getString(4));
                bus.setRuta(resultado.getString(5));

            }

            return bus;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en bus", JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error desconocido", "Error en bus", JOptionPane.ERROR_MESSAGE);

        }

        return null;
    }

    public ArrayList<Bus> extraerTodosBuses() {
        ArrayList<Bus> todosBuses = new ArrayList<>();

        String sql_select;
        sql_select = "SELECT id_bus,numero_interno_bus,placa_bus,tipo_bus,id_ruta "
                + "FROM  buses;";
        try {
            Connection conn = fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql_select);

            while (resultado.next()) {
                Bus bus = new Bus();
                bus.setId(resultado.getString(1));
                bus.setNumero(resultado.getString(2));
                bus.setPlaca(resultado.getString(3));
                bus.setTipo(resultado.getString(4));
                bus.setRuta(resultado.getString(5));
                todosBuses.add(bus);

            }

            return todosBuses;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en bus", JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error desconocido", "Error en bus", JOptionPane.ERROR_MESSAGE);

        }

        return todosBuses;
    }

    public String actualizarRuta(String placa, String numeroInterno, String id_ruta) {
        String mensaje = "", query;
        int resultado;
        query = "UPDATE buses set (id_ruta) "
                + "= ('" + id_ruta + "')"
                + " WHERE numero_interno_bus='" + numeroInterno + "' AND placa_bus = '" + placa + "';";

        try {
            Connection conexion = this.fachada.getConexion();
            Statement sentencia = conexion.createStatement();
            resultado = sentencia.executeUpdate(query);
            if (resultado == 1) {
                mensaje = "Registro Actualizado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar la actualización :" + ex.getMessage();
        }

        return mensaje;
    }

    public ResultSet consultarBusesArtculadosConductores() {
        String sql_select;
        sql_select = " SELECT numero_interno_bus,placa_bus,nombre_ruta,cedula_empleado,\n"
                + "	 nombre_empleado,telefono_empleado,email_empleado\n"
                + " FROM buses INNER JOIN rutas ON buses.id_ruta = rutas.id_ruta \n"
                + " INNER JOIN turnos ON buses.id_bus =turnos.id_bus\n"
                + " INNER JOIN empleados ON empleados.id_empleado = turnos.id_empleado "
                + " WHERE tipo_bus ILIKE 'articulado'";

        try {
            Connection conn = fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql_select);

            return resultado;

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error", JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error desconocido", "Error en ruta", JOptionPane.ERROR_MESSAGE);

        }

        return null;
    }

}
