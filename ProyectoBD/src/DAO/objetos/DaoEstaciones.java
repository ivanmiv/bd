/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.objetos;

import AccesoDatos.Fachada;
import DAO.logica.Estacion;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author flavio
 */
public class DaoEstaciones {
    Fachada fachada;

    public DaoEstaciones() {
        this.fachada = new Fachada();
    }
    public String IngresarEstacion(Estacion estacion){
        String mensaje="",query;
        int resultado;
        query= "INSERT INTO estaciones (nombre_estacion,ubicacion_estacion,id_director,estado_estacion )"
                + "VALUES ('" +estacion.getNombre()+ "', '" + estacion.getUbicacion()+  "', '" +
                estacion.getDirector()+"', '"+estacion.getEstado()+"');";
        
        
        try {
            Connection conexion = this.fachada.getConexion();
            Statement sentencia = conexion.createStatement();
            resultado = sentencia.executeUpdate(query);
            if (resultado == 1) {
                mensaje = "Registro ingresado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar el registro :" + ex.getMessage();
        }
    
        return mensaje;
    }
    
    public Estacion ConsultarEstacion(String identificacion){
        
        Estacion estacion= new Estacion();
        String sql_select;
        sql_select="SELECT id_estacion,nombre_estacion,ubicacion_estacion,id_director,estado_estacion "
                + "FROM  estaciones WHERE id_estacion='" + identificacion +  "'";
         try{
            Connection conn= fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql_select);
            
            while(resultado.next()){
                
               estacion.setId(resultado.getString(1));
               estacion.setNombre(resultado.getString(2));
               estacion.setUbicacion(resultado.getString(3));
               estacion.setDirector(resultado.getString(4));
               estacion.setEstado(resultado.getString(5));
            }
           
            return estacion;
         }
         catch(SQLException e){ 
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en estacion",JOptionPane.ERROR_MESSAGE );
         }
         catch(Exception e){
            JOptionPane.showMessageDialog(null, "Error desconocido", "Error en estacion",JOptionPane.ERROR_MESSAGE );

         }
   
        return null;
    }
    
     public Estacion ConsultarEstacionNombre(String identificacion){
        
        Estacion estacion= new Estacion();
        String sql_select;
        sql_select="SELECT id_estacion,nombre_estacion,ubicacion_estacion,id_director,estado_estacion "
                + "FROM  estaciones WHERE nombre_estacion='" + identificacion +  "'";
         try{
            Connection conn= fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql_select);
            
            while(resultado.next()){
                
               estacion.setId(resultado.getString(1));
               estacion.setNombre(resultado.getString(2));
               estacion.setUbicacion(resultado.getString(3));
               estacion.setDirector(resultado.getString(4));
               estacion.setEstado(resultado.getString(5));
            }
           
            return estacion;
         }
         catch(SQLException e){ 
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en estacion",JOptionPane.ERROR_MESSAGE );
         }
         catch(Exception e){
            JOptionPane.showMessageDialog(null, "Error desconocido", "Error en estacion",JOptionPane.ERROR_MESSAGE );

         }
   
        return null;
    }
    
    public String EliminarEstacion(String identificacion){
        String mensaje="",query;
        int resultado;
        query= "DELETE FROM estaciones WHERE id_estacion='" + identificacion +  "';";
        
        
        try {
            Connection conexion = this.fachada.getConexion();
            Statement sentencia = conexion.createStatement();
            resultado = sentencia.executeUpdate(query);
            if (resultado == 1) {
                mensaje = "Registro eliminado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar la eliminación :" + ex.getMessage();
        }
    
        return mensaje;
    
    }
    
    public String ActualizarEstacion(Estacion estacion){
        String mensaje="",query;
        int resultado;
        query= "UPDATE estaciones set (nombre_estacion,ubicacion_estacion,id_director,estado_estacion) "
                + "= ('" +estacion.getNombre()+ "', '" + estacion.getUbicacion()+  "', '" +
                estacion.getDirector()+"', '"+estacion.getEstado()+"')"
                + " WHERE id_estacion ='" + estacion.getId()+"';";
        
        
        try {
            Connection conexion = this.fachada.getConexion();
            Statement sentencia = conexion.createStatement();
            resultado = sentencia.executeUpdate(query);
            if (resultado == 1) {
                mensaje = "Registro Actualizado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar la actualización :" + ex.getMessage();
        }
    
        return mensaje;
    }
    
    public ArrayList<Estacion> MostrarEstaciones(){
        
        ArrayList<Estacion> estaciones = new ArrayList<Estacion>();
        
        String sql_select;
        sql_select="SELECT id_estacion,nombre_estacion,ubicacion_estacion,id_director,estado_estacion "
                + "FROM  estaciones";
         try{
            Connection conn= fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql_select);
            
            while(resultado.next()){
               Estacion estaciontemp = new Estacion();
                
               estaciontemp.setId(resultado.getString(1));
               estaciontemp.setNombre(resultado.getString(2));
               estaciontemp.setUbicacion(resultado.getString(3));
               estaciontemp.setDirector(resultado.getString(4));
               estaciontemp.setEstado(resultado.getString(5));
               
               estaciones.add(estaciontemp);
            }
           
           
         }
         catch(SQLException e){ 
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en estacion",JOptionPane.ERROR_MESSAGE );
         }
         catch(Exception e){
            JOptionPane.showMessageDialog(null, "Error desconocido", "Error en estacion",JOptionPane.ERROR_MESSAGE );

         }
    
        
        return estaciones;
    }
    
    public ArrayList<Estacion> MostrarEstacionesRuta(String ruta){
        
        ArrayList<Estacion> estaciones = new ArrayList<Estacion>();
        
        String sql_select;
        sql_select="select E.nombre_estacion,R.nombre_ruta From atender as A INNER JOIN estaciones as E ON (A.id_estacion = E.id_estacion) " +
                    "INNER JOIN rutas as R On (R.id_ruta = A.id_ruta) WHERE R.nombre_ruta = '"+ruta+"'";
         try{
            Connection conn= fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql_select);
            
            while(resultado.next()){
               Estacion estaciontemp = new Estacion();
                
              
               estaciontemp.setNombre(resultado.getString(1));
              
               
               estaciones.add(estaciontemp);
            }
           
           
         }
         catch(SQLException e){ 
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en estacion",JOptionPane.ERROR_MESSAGE );
         }
         catch(Exception e){
            JOptionPane.showMessageDialog(null, "Error desconocido", "Error en estacion",JOptionPane.ERROR_MESSAGE );

         }
    
        
        return estaciones;
    }
    
}
