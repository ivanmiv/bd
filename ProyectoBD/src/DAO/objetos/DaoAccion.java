/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.objetos;

import AccesoDatos.Fachada;
import DAO.logica.Accion;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author dacer
 */
public class DaoAccion {
    Fachada fachada;

    public DaoAccion() {  
        this.fachada =  new Fachada();
    }
    
    
    public String IngresarAccion(String id,String accion) {
        String mensaje = "", query;
        int resultado;
        query = "INSERT INTO accionespqrs (id_solicitud,accion_solicitud)"
                + " VALUES ('" + id + "', '" + accion+ "');";

        try {
            Connection conexion = this.fachada.getConexion();
            Statement sentencia = conexion.createStatement();
            resultado = sentencia.executeUpdate(query);
            if (resultado == 1) {
                mensaje = "Registro ingresado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar el registro :" + ex.getMessage();
        }

        return mensaje;
    }
    
    public ArrayList<String> TraerListaAcciones(String identificador){
        ArrayList<String> acciones = new ArrayList<>();
        
        String sql_select;
        
        sql_select = "SELECT id_solicitud,accion_solicitud "
                + "FROM  accionesPQRS WHERE id_solicitud='" + identificador+"';";
        System.out.println(sql_select);
       
        try{
           
            Connection conn= fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql_select);
            
            while(resultado.next()){
                
               acciones.add(resultado.getString(2)) ;
               
            }
           
           
         } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en solicitud", JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error desconocido", "Error en solicitud", JOptionPane.ERROR_MESSAGE);
        }
        
        return acciones;
    }
   /* 
    public Accion ConsultarSolicitud(String id, String acc) {

        Accion accion = new Accion();
        String sql_select;
        sql_select = "SELECT id_solicitud,accion_solicitud"
                + " FROM  accionespqrs WHERE id_solicitud='" + id + "' AND accion_solicitud='";";
        System.out.println(sql_select);
        try {
            Connection conn = fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql_select);
            //String sql_select2 = "SELECT * FROM  accionesPQRS WHERE id_solicitud = '" + identificacion + "';";
            //ResultSet resultado2 = sentencia.executeQuery(sql_select2);

            while (resultado.next()) {
                

                solicitud.setId(resultado.getString(1));
                
                solicitud.setMotivo(resultado.getString(2));
                solicitud.setIdescripcion(resultado.getString(3));
                solicitud.setFechaSolicitud(resultado.getString(4));
                solicitud.setEstado(resultado.getString(5));

                solicitud.setPasajero(resultado.getString(6));
                solicitud.setEstacion(resultado.getString(7));

            }
            
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en solicitud", JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error desconocido", "Error en solicitud", JOptionPane.ERROR_MESSAGE);
        }
              

        return solicitud;
        
    } */
    
}
