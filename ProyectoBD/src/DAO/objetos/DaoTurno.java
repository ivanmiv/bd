/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.objetos;

import AccesoDatos.Fachada;
import DAO.logica.Turno;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author dacer
 */
public class DaoTurno {
    Fachada fachada;

    public DaoTurno() {
        this.fachada =  new Fachada();
    }
    
    
    public String IngresarTurno(Turno turno){
       String mensaje="",query;
        int resultado;
        query= "INSERT INTO turnos (id_empleado,id_bus ,fecha_inicio_turno,fecha_fin_turno,jornada_turno  )"
                + "VALUES ('" +turno.getEmpleado()+"', '" +turno.getBus()+"', '" +turno.getFechaInicio()
                +"', '" +turno.getFechaFin()+"', '" +turno.getJornada()+"');";
        System.out.print(query);
        
        try {
            Connection conexion = this.fachada.getConexion();
            Statement sentencia = conexion.createStatement();
            resultado = sentencia.executeUpdate(query);
            if (resultado == 1) {
                mensaje = "Registro ingresado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar el registro :" + ex.getMessage();
        }
    
        return mensaje;
    
    
    }
    
    public String ActualizarTurno(Turno viejo,Turno nuevo){
         String mensaje="",query;
        int resultado;
        query= "UPDATE turnos set(id_bus,fecha_inicio_turno,fecha_fin_turno,jornada_turno )=('" +
                nuevo.getBus()+ "', '" +nuevo.getFechaInicio()+ "', '" + nuevo.getFechaFin()+  "', '" +
                nuevo.getJornada()+"')"
                + " WHERE id_empleado ='" +  viejo.getEmpleado()+"' and id_bus= '"+viejo.getBus()+"' and fecha_inicio_turno ='"
                +viejo.getFechaInicio()+ "' and fecha_fin_turno = '"+viejo.getFechaFin()+"' and jornada_turno='"
                +viejo.getJornada()+"'";
        
        
        try {
            Connection conexion = this.fachada.getConexion();
            Statement sentencia = conexion.createStatement();
            resultado = sentencia.executeUpdate(query);
            if (resultado == 1) {
                mensaje = "Registro Actualizado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar la actualización :" + ex.getMessage();
        }
    
        return mensaje;
        
        
    
        
    }
    
    public ResultSet TraerTunos(){
    
        String sql_select;
        sql_select="Select E.nombre_empleado,E.cedula_empleado,B.numero_interno_bus,T.fecha_inicio_turno,T.fecha_fin_turno,T.jornada_turno"
                + " from turnos as T INNER JOIN empleados as "
                + "E ON (T.id_empleado = E.id_empleado) INNER JOIN buses as B ON (B.id_bus=T.id_bus) ORDER BY E.nombre_empleado";
        ResultSet resultado =null ;
         try{
            Connection conn= fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            resultado = sentencia.executeQuery(sql_select);
            
           
           
           
         }
         catch(SQLException e){ 
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en buses",JOptionPane.ERROR_MESSAGE );
            
         }
         catch(Exception e){
            JOptionPane.showMessageDialog(null, "Error desconocido", "Error en empleado",JOptionPane.ERROR_MESSAGE );
            

         }  
         
          return resultado;
    }
    
    
    public Turno ConsultarturnoUser(String empleado){
        Turno turno= new Turno();
        String sql_select;
        sql_select="SELECT id_empleado,id_bus,fecha_inicio_turno,fecha_fin_turno,jornada_turno "
                + "FROM  turnos WHERE id_empleado='" + empleado +  "' ORDER BY id_empleado DESC LIMIT 1;";
         try{
            Connection conn= fachada.getConexion();
            System.out.println("consultando en la bd");
            Statement sentencia = conn.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql_select);
            
            while(resultado.next()){
                
               turno.setEmpleado(resultado.getString(1));
               turno.setBus(resultado.getString(2));
               turno.setFechaInicio(resultado.getString(3));
               turno.setFechaFin(resultado.getString(4));
               turno.setJornada(resultado.getString(5));
               
               
            }
           
           
         }
         catch(SQLException e){ 
            JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta", "Error en empleado",JOptionPane.ERROR_MESSAGE );
            empleado=null;
         }
         catch(Exception e){
            JOptionPane.showMessageDialog(null, "Error desconocido", "Error en empleado",JOptionPane.ERROR_MESSAGE );
            empleado=null;

         }  
         
          return turno;
    
        
    }
    
}
