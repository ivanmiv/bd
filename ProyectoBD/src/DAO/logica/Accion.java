/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.logica;

/**
 *
 * @author dacer
 */
    

public class Accion {
    private String id;
    private String accion;

    public Accion() {
    }
    
    

    public String getId() {
        return id;
    }
  

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public void setId(String id) {
        this.id = id;
    }
}
