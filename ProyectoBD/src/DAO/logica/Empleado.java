
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.logica;

/**
 *
 * @author flavio
 */
public class Empleado {
    
    private String id;
    private String cedula;
    private String nombre;
    private String telefono;
    private String direccion;
    private String email;
    private String cargo;
    private int salario;
    private String idEstacion;
    private String estado;
    private String usuario;
    private String contrasenha;

    
    public Empleado() {
    }
    
    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasenha() {
        return contrasenha;
    }

    public void setContrasenha(String contrasenha) {
        this.contrasenha = contrasenha;
    }

    //metodos set de empleado
    public void setId(String id) {
        this.id = id;
    }
    
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public void setSalario(int salario) {
        this.salario = salario;
    }

    public void setIdEstacion(String idEstacion) {
        this.idEstacion = idEstacion;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    //metodos get empleado
    public String getId() {
        return id;
    }
    public String getCedula() {
        return cedula;
    }
    public String getNombre() {
        return nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getEmail() {
        return email;
    }

    public String getCargo() {
        return cargo;
    }

    public int getSalario() {
        return salario;
    }

    public String getIdEstacion() {
        return idEstacion;
    }

    public String getEstado() {
        return estado;
    }   

    
}
