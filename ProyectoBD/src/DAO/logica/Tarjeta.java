/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.logica;

/**
 *
 * @author flavio
 */
public class Tarjeta {
    private String pin;
    private int saldo;
    private String estado;
    private String estacion_id_creacion;

    public String getEstacion_id_creacion() {
        return estacion_id_creacion;
    }

    public void setEstacion_id_creacion(String estacion_id_creacion) {
        this.estacion_id_creacion = estacion_id_creacion;
    }

    public Tarjeta() {
    }
//Getters de Tarjeta
    public String getPin() {
        return pin;
    }

    public int getSaldo() {
        return saldo;
    }

    public String getEstado() {
        return estado;
    }
//Setter de Tarjeta
    public void setPin(String pin) {
        this.pin = pin;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    
}
