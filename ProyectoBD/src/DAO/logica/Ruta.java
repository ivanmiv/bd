/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.logica;

/**
 *
 * @author flavio
 */
public class Ruta {
    private String id;
    private String nombre;
    private String desscripcion;

    public void setId(String id) {
        this.id = id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDesscripcion(String desscripcion) {
        this.desscripcion = desscripcion;
    }

    public Ruta() {
    }

    public String getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDescripcion() {
        return desscripcion;
    }
       
}
