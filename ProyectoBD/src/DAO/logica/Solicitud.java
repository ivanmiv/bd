/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.logica;

import java.util.ArrayList;

/**
 *
 * @author flavio
 */
public class Solicitud {
    
    private String id;
    private String motivo;
    private String idescripcion;
    private String fechaSolicitud;
    private String estado;
    private ArrayList<String> accionesTomadas;
    private String pasajero;
    private String estacion;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getIdescripcion() {
        return idescripcion;
    }

    public void setIdescripcion(String idescripcion) {
        this.idescripcion = idescripcion;
    }

    public String getFechaSolicitud() {
        return fechaSolicitud;
    }

    public void setFechaSolicitud(String fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public ArrayList<String> getAccion() {
        return accionesTomadas;
    }

    public void addAcciones(ArrayList<String> accion) {
        accionesTomadas = accion;
    }
    
    public void setAccion(String posicion,  String valor) {
        int pos = Integer.parseInt(posicion);
        accionesTomadas.set(pos, valor);
    }

    public String getPasajero() {
        return pasajero;
    }

    public void setPasajero(String pasajero) {
        this.pasajero = pasajero;
    }

    public String getEstacion() {
        return estacion;
    }

    public void setEstacion(String estacion) {
        this.estacion = estacion;
    }
    
}
