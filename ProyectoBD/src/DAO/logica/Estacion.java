/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.logica;

/**
 *
 * @author flavio
 */
public class Estacion {
    private String id;
    private String nombre;
    private String ubicacion;
    private String director;
    private String estado;

    public Estacion() {
    }

    //setters de estacion
    public void setId(String id) {
        this.id = id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    //getters de Estacion
    public String getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public String getDirector() {
        return director;
    }

    public String getEstado() {
        return estado;
    }
}
