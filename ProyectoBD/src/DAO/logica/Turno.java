/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.logica;

/**
 *
 * @author dacer
 */
public class Turno {
    
    private String empleado;
    private String bus;
    private String fechaInicio;
    private String fechaFin;
    private String jornada;

    public Turno() {
    }
    
    public void setEmpleado(String empleado) {
        this.empleado = empleado;
    }

    public void setBus(String bus) {
        this.bus = bus;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public void setJornada(String jornada) {
        this.jornada = jornada;
    }

    public String getEmpleado() {
        return empleado;
    }

    public String getBus() {
        return bus;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public String getJornada() {
        return jornada;
    }
}
