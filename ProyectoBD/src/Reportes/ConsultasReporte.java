
/*  
 * 
 * 
 * 
 */
package Reportes;

import AccesoDatos.Fachada;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Clase encargada de entregar datos al modelo
 *
 */
public class ConsultasReporte {

    private Connection conexion;
    private ResultSet resultado;
    private Statement sentencia;

    public ConsultasReporte() {

    }

    ResultSet ConsultarUsuarios() {

        ResultSet resultado = null;
        String consulta = "SELECT cedula_pasajero,nombre_pasajero,telefono_pasajero,pin_tarjeta FROM pasajeros;";

        resultado = EjecutarConsultaReporte(consulta);
        return resultado;

    }

    ResultSet consultarCantidadPasajeros(int anho) {
        ResultSet resultado = null;
        String consulta = " SELECT nombre_ruta elemento,COALESCE(cantidad,0) cantidad "
                + " FROM rutas "
                + " LEFT JOIN (SELECT id_ruta, COUNT(*) cantidad  FROM registrar_pasaje \n"
                + " WHERE EXTRACT(YEAR FROM fecha_hora) = '" + anho + "' GROUP BY id_ruta ) pasajes_ruta   "
                + " ON rutas.id_ruta = pasajes_ruta.id_ruta"
                + " ORDER BY nombre_ruta;";

        resultado = EjecutarConsultaReporte(consulta);
        return resultado;
    }

    ResultSet consultarCantidadPasajeros(int anho, String ruta) {
        ResultSet resultado = null;
        String consulta = "SELECT  todos_meses.mes elemento, COALESCE(cantidad,0) cantidad\n"
                + " FROM (SELECT 1 as mes UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION \n"
                + " ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9 UNION ALL SELECT 10 UNION ALL SELECT 11\n"
                + " UNION ALL SELECT 12)  todos_meses  LEFT JOIN  (SELECT COUNT(fecha_hora) cantidad, EXTRACT(MONTH FROM fecha_hora) mes \n"
                + " FROM registrar_pasaje WHERE EXTRACT(YEAR FROM fecha_hora) = '" + anho + "'     AND id_ruta = (SELECT id_ruta FROM rutas WHERE nombre_ruta = '" + ruta + "' )\n"
                + " GROUP BY  EXTRACT(YEAR FROM fecha_hora),EXTRACT(MONTH FROM fecha_hora)) cantidad_usuarios  ON cantidad_usuarios.mes = todos_meses.mes;";

        resultado = EjecutarConsultaReporte(consulta);
        return resultado;
    }

    ResultSet consultarCantidadPasajeros(int anho, int mes) {
        ResultSet resultado = null;
        String consulta = " SELECT nombre_ruta elemento,COALESCE(cantidad,0) cantidad "
                + " FROM rutas "
                + " LEFT JOIN (SELECT id_ruta, COUNT(*) cantidad  FROM registrar_pasaje \n"
                + " WHERE EXTRACT(YEAR FROM fecha_hora) = '" + anho + "'"
                + " AND EXTRACT(MONTH FROM fecha_hora) = '" + mes + "'"
                + " GROUP BY id_ruta ) pasajes_ruta   "
                + " ON rutas.id_ruta = pasajes_ruta.id_ruta"
                + " ORDER BY nombre_ruta;";

        resultado = EjecutarConsultaReporte(consulta);
        return resultado;
    }

    ResultSet consultarCantidadPasajeros(int anho, int mes, String ruta) {

        ResultSet resultado = null;
        Calendar cal = new GregorianCalendar(anho, mes - 1, 1);
        // Get the number of days in that month 
        int dias = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

        String subConsultaDias = "SELECT 1 as dia  ";
        for (int i = 2; i <= dias; i++) {
            subConsultaDias += " UNION ALL SELECT " + i + " \n ";
        }

        String consulta = "SELECT COALESCE(cantidad,0) cantidad , dias.dia elemento  FROM "
                + "( " + subConsultaDias + " ) dias"
                + " LEFT JOIN  (SELECT COUNT(fecha_hora) cantidad, EXTRACT(DAY FROM fecha_hora) dia \n"
                + "   FROM registrar_pasaje WHERE EXTRACT(YEAR FROM fecha_hora) = '" + anho + "'   \n"
                + "   AND  EXTRACT(MONTH FROM fecha_hora) = '" + mes + "'      \n"
                + "   AND id_ruta = (SELECT id_ruta FROM rutas WHERE nombre_ruta = '" + ruta + "' )\n"
                + "   GROUP BY  EXTRACT(YEAR FROM fecha_hora),EXTRACT(MONTH FROM fecha_hora),\n"
                + "	     EXTRACT(DAY FROM fecha_hora) ) cantidad_usuarios  \n"
                + "    ON cantidad_usuarios.dia= dias.dia;";

        resultado = EjecutarConsultaReporte(consulta);
        return resultado;
    }

    ResultSet consultarValorVentasTarjetas(int anho) {
        ResultSet resultado = null;
        String consulta = "SELECT  e.nombre_estacion elemento, COALESCE(cantidad,0) cantidad\n"
                + "FROM\n"
                + "(SELECT nombre_estacion FROM estaciones)  e\n"
                + "LEFT JOIN\n"
                + "(SELECT nombre_estacion elemento, COUNT(*)*3000 cantidad\n"
                + "FROM tarjetas INNER JOIN estaciones ON tarjetas.estacion_id = estaciones.id_estacion\n"
                + "WHERE EXTRACT (YEAR FROM fecha_venta) = '" + anho + "'\n"
                + "GROUP BY id_estacion)valor_tarjetas\n"
                + "ON e.nombre_estacion = valor_tarjetas.elemento\n"
                + ";";

        resultado = EjecutarConsultaReporte(consulta);
        return resultado;
    }

    ResultSet consultarValorVentasTarjetas(int anho, int mes) {
        ResultSet resultado = null;
        String consulta = "SELECT  e.nombre_estacion elemento, COALESCE(cantidad,0) cantidad\n"
                + "FROM\n"
                + "(SELECT nombre_estacion FROM estaciones)  e\n"
                + "LEFT JOIN\n"
                + "(SELECT nombre_estacion elemento, COUNT(*)*3000 cantidad\n"
                + "FROM tarjetas INNER JOIN estaciones ON tarjetas.estacion_id = estaciones.id_estacion\n"
                + "WHERE EXTRACT (YEAR FROM fecha_venta) = '" + anho + "'\n"
                + "AND EXTRACT (MONTH FROM fecha_venta) = '" + mes + "'\n"
                + "GROUP BY id_estacion)valor_tarjetas\n"
                + "ON e.nombre_estacion = valor_tarjetas.elemento\n"
                + ";";

        resultado = EjecutarConsultaReporte(consulta);
        return resultado;
    }
    
    
    ResultSet consultarValorVentasTarjetas(int anho, int mes, int dia) {
        ResultSet resultado = null;
        String consulta = "SELECT  e.nombre_estacion elemento, COALESCE(cantidad,0) cantidad\n"
                + "FROM\n"
                + "(SELECT nombre_estacion FROM estaciones)  e\n"
                + "LEFT JOIN\n"
                + "(SELECT nombre_estacion elemento, COUNT(*)*3000 cantidad\n"
                + "FROM tarjetas INNER JOIN estaciones ON tarjetas.estacion_id = estaciones.id_estacion\n"
                + "WHERE EXTRACT (YEAR FROM fecha_venta) = '" + anho + "'\n"
                + "AND EXTRACT (MONTH FROM fecha_venta) = '" + mes + "'\n"
                + "AND EXTRACT (DAY FROM fecha_venta) = '" + dia + "'\n"
                + "GROUP BY id_estacion)valor_tarjetas\n"
                + "ON e.nombre_estacion = valor_tarjetas.elemento\n"
                + ";";

        resultado = EjecutarConsultaReporte(consulta);
        return resultado;
    }

    ResultSet consultarQuejas(int anho) {
        ResultSet resultado = null;
        String consulta = " SELECT nombre_pasajero,descripcion_solicitud,nombre_estacion,nombre_empleado \n"
                + " FROM \n"
                + " solicitudPQRS INNER JOIN pasajeros\n"
                + " ON pasajeros.id_pasajero = solicitudPQRS.pasajero_id \n"
                + " INNER JOIN estaciones ON solicitudPQRS.estacion_id = estaciones.id_estacion\n"
                + " INNER JOIN empleados ON  solicitudPQRS.id_empleado = empleados.id_empleado\n"
                + " WHERE EXTRACT(YEAR FROM fecha_solicitud) = '"+anho+"'\n"
                + ";";

        resultado = EjecutarConsultaReporte(consulta);
        return resultado;
    }

    ResultSet consultarQuejas(int anho, String estacion) {
        ResultSet resultado = null;
        String consulta = " SELECT nombre_pasajero,descripcion_solicitud,nombre_estacion,nombre_empleado \n"
                + " FROM \n"
                + " solicitudPQRS INNER JOIN pasajeros\n"
                + " ON pasajeros.id_pasajero = solicitudPQRS.pasajero_id \n"
                + " INNER JOIN estaciones ON solicitudPQRS.estacion_id = estaciones.id_estacion\n"
                + " INNER JOIN empleados ON  solicitudPQRS.id_empleado = empleados.id_empleado\n"
                + " WHERE EXTRACT(YEAR FROM fecha_solicitud) = '"+anho+"'\n"
                + " AND nombre_estacion ILIKE '"+estacion+"'\n"
                + ";";

        resultado = EjecutarConsultaReporte(consulta);
        return resultado;
    }

    ResultSet consultarQuejas(int anho, int mes) {
        ResultSet resultado = null;
        String consulta = " SELECT nombre_pasajero,descripcion_solicitud,nombre_estacion,nombre_empleado \n"
                + " FROM \n"
                + " solicitudPQRS INNER JOIN pasajeros\n"
                + " ON pasajeros.id_pasajero = solicitudPQRS.pasajero_id \n"
                + " INNER JOIN estaciones ON solicitudPQRS.estacion_id = estaciones.id_estacion\n"
                + " INNER JOIN empleados ON  solicitudPQRS.id_empleado = empleados.id_empleado\n"
                + " WHERE EXTRACT(YEAR FROM fecha_solicitud) = '"+anho+"'\n"
                + " AND EXTRACT(MONTH FROM fecha_solicitud) = '"+mes+"'\n"
                + ";";

        resultado = EjecutarConsultaReporte(consulta);
        return resultado;
    }

    ResultSet consultarQuejas(int anho, int mes, String estacion) {
        ResultSet resultado = null;
        String consulta = " SELECT nombre_pasajero,descripcion_solicitud,nombre_estacion,nombre_empleado \n"
                + " FROM \n"
                + " solicitudPQRS INNER JOIN pasajeros\n"
                + " ON pasajeros.id_pasajero = solicitudPQRS.pasajero_id \n"
                + " INNER JOIN estaciones ON solicitudPQRS.estacion_id = estaciones.id_estacion\n"
                + " INNER JOIN empleados ON  solicitudPQRS.id_empleado = empleados.id_empleado\n"
                + " WHERE EXTRACT(YEAR FROM fecha_solicitud) = '"+anho+"'\n"
                + " AND EXTRACT(MONTH FROM fecha_solicitud) = '"+mes+"'\n"
                + " AND nombre_estacion ILIKE '"+estacion+"'\n"
                + ";";

        resultado = EjecutarConsultaReporte(consulta);
        return resultado;
    }

    ResultSet consultarQuejas(int anho, int mes, int dia, String estacion) {
        ResultSet resultado = null;
        String consulta = " SELECT nombre_pasajero,descripcion_solicitud,nombre_estacion,nombre_empleado \n"
                + " FROM \n"
                + " solicitudPQRS INNER JOIN pasajeros\n"
                + " ON pasajeros.id_pasajero = solicitudPQRS.pasajero_id \n"
                + " INNER JOIN estaciones ON solicitudPQRS.estacion_id = estaciones.id_estacion\n"
                + " INNER JOIN empleados ON  solicitudPQRS.id_empleado = empleados.id_empleado\n"
                + " WHERE EXTRACT(YEAR FROM fecha_solicitud) = '"+anho+"'\n"
                + " AND EXTRACT(MONTH FROM fecha_solicitud) = '"+mes+"'\n"
                + " AND  EXTRACT(DAY FROM fecha_solicitud) = '"+dia+"'\n"
                + " AND nombre_estacion ILIKE '"+estacion+"'\n"
                + ";";

        resultado = EjecutarConsultaReporte(consulta);
        return resultado;
    }

    ResultSet consultarQuejas(int anho, int mes, int dia) {
        ResultSet resultado = null;
        String consulta = " SELECT nombre_pasajero,descripcion_solicitud,nombre_estacion,nombre_empleado \n"
                + " FROM \n"
                + " solicitudPQRS INNER JOIN pasajeros\n"
                + " ON pasajeros.id_pasajero = solicitudPQRS.pasajero_id \n"
                + " INNER JOIN estaciones ON solicitudPQRS.estacion_id = estaciones.id_estacion\n"
                + " INNER JOIN empleados ON  solicitudPQRS.id_empleado = empleados.id_empleado\n"
                + " WHERE EXTRACT(YEAR FROM fecha_solicitud) = '"+anho+"'\n"
                + " AND EXTRACT(MONTH FROM fecha_solicitud) = '"+mes+"'\n"
                + " AND  EXTRACT(DAY FROM fecha_solicitud) = '"+dia+"'\n"
                + ";";

        resultado = EjecutarConsultaReporte(consulta);
        return resultado;
    }

    

    private ResultSet EjecutarConsultaReporte(String consulta) {
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion = fachada.getConexion();
        ResultSet resultado = null;

        System.out.println(consulta);
        try {

            this.sentencia = this.conexion.createStatement();

            this.resultado = this.sentencia.executeQuery(consulta);

            if (this.resultado.isBeforeFirst()) {

                resultado = this.resultado;
            }

        } catch (SQLException ex) {
            System.out.println("No se pudo realizar la consulta :" + ex.getMessage());
        }
        fachada.CerrarBd();
        return resultado;
    }

}
