/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Reportes;

import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Ivan
 */
public class ReportesVentasTarjetas {

    public ReportesVentasTarjetas(int anho) {

        ResultSet resultado = null;
        JOptionPane mensaje = new JOptionPane();
        int result = mensaje.showConfirmDialog(null, "Generando reporte",
                "", JOptionPane.OK_CANCEL_OPTION);

        if (result == 2 || result == -1) {
            return;
        }
        try {

            resultado = new ConsultasReporte().consultarValorVentasTarjetas(anho);

            if (resultado.getRow() == 0) {

                System.out.println("vacio");
            } else {
                System.out.println("no vacio");
            }
            InputStream template = getClass().getResourceAsStream("/Reportes/ReporteAnhoMesDiaGeneral.jasper");

            JasperReport reporte = (JasperReport) JRLoader.loadObject(template);

           Map param = new HashMap();
            param.put("TITULO_REPORTE", "VALOR DE TARJETAS VENDIDAS EN  EL AÑO " + anho );
            param.put("TITULO_COLUMNA1", "ESTACION");
            param.put("TITULO_COLUMNA2", "VALOR DE TARJETAS VENDIDAS");
            param.put("TITULO_X", "VALOR DE TARJETAS VENDIDAS");
            param.put("TITULO_Y", "ESTACION");
            param.put("TITULO_GRAFICO", "");
            JasperPrint jasperprint = JasperFillManager.fillReport(reporte, param, new JRResultSetDataSource(resultado));
            String titulo = "Tarjetas Vendidas-" + anho ;
            JasperExportManager.exportReportToPdfFile(jasperprint, titulo + ".pdf");

            JasperViewer visor = new JasperViewer(jasperprint, false);
            visor.setTitle(titulo);
            visor.setVisible(true);
            visor.setFitWidthZoomRatio();

        } catch (JRException ex) {
            System.out.println("Error generando reporte " + ex);

        } catch (NullPointerException e) {
            System.err.println(e);

            JOptionPane.showMessageDialog(null, "No se encontraron resultados");
        } catch (SQLException ex) {
            System.err.println(ex);
        }

    }

    public ReportesVentasTarjetas(int anho, int mes) {

        ResultSet resultado = null;
        JOptionPane mensaje = new JOptionPane();
        int result = mensaje.showConfirmDialog(null, "Generando reporte",
                "", JOptionPane.OK_CANCEL_OPTION);

        if (result == 2 || result == -1) {
            return;
        }
        try {

            resultado = new ConsultasReporte().consultarValorVentasTarjetas(anho, mes);

            if (resultado.getRow() == 0) {

                System.out.println("vacio");
            } else {
                System.out.println("no vacio");
            }
            InputStream template = getClass().getResourceAsStream("/Reportes/ReporteAnhoMesDiaGeneral.jasper");

            JasperReport reporte = (JasperReport) JRLoader.loadObject(template);

            Map param = new HashMap();
            param.put("TITULO_REPORTE", "VALOR DE TARJETAS VENDIDAS EN  EL AÑO " + anho + " MES " + mes);
            param.put("TITULO_COLUMNA1", "ESTACION");
            param.put("TITULO_COLUMNA2", "VALOR DE TARJETAS VENDIDAS");
            param.put("TITULO_X", "VALOR DE TARJETAS VENDIDAS");
            param.put("TITULO_Y", "ESTACION");
            param.put("TITULO_GRAFICO", "");
            JasperPrint jasperprint = JasperFillManager.fillReport(reporte, param, new JRResultSetDataSource(resultado));
            String titulo = "Tarjetas Vendidas-" + anho + "-" + mes;
            JasperExportManager.exportReportToPdfFile(jasperprint, titulo + ".pdf");

            JasperViewer visor = new JasperViewer(jasperprint, false);
            visor.setTitle(titulo);
            visor.setVisible(true);
            visor.setFitWidthZoomRatio();

        } catch (JRException ex) {
            System.out.println("Error generando reporte " + ex);

        } catch (NullPointerException e) {
            System.err.println(e);

            JOptionPane.showMessageDialog(null, "No se encontraron resultados");
        } catch (SQLException ex) {
            System.err.println(ex);
        }

    }

    public ReportesVentasTarjetas(int anho, int mes,int dia) {

        ResultSet resultado = null;
        JOptionPane mensaje = new JOptionPane();
        int result = mensaje.showConfirmDialog(null, "Generando reporte",
                "", JOptionPane.OK_CANCEL_OPTION);

        if (result == 2 || result == -1) {
            return;
        }
        try {

            resultado = new ConsultasReporte().consultarValorVentasTarjetas(anho, mes, dia);

            if (resultado.getRow() == 0) {

                System.out.println("vacio");
            } else {
                System.out.println("no vacio");
            }
            InputStream template = getClass().getResourceAsStream("/Reportes/ReporteAnhoMesDiaGeneral.jasper");

            JasperReport reporte = (JasperReport) JRLoader.loadObject(template);

            Map param = new HashMap();
            param.put("TITULO_REPORTE", "VALOR DE TARJETAS VENDIDAS EN  EL AÑO " + anho + " MES " + mes+ " DIA "+dia);
            param.put("TITULO_COLUMNA1", "ESTACION");
            param.put("TITULO_COLUMNA2", "VALOR DE TARJETAS VENDIDAS");
            param.put("TITULO_X", "VALOR DE TARJETAS VENDIDAS");
            param.put("TITULO_Y", "ESTACION");
            param.put("TITULO_GRAFICO", "");
            JasperPrint jasperprint = JasperFillManager.fillReport(reporte, param, new JRResultSetDataSource(resultado));
            String titulo = "Tarjetas Vendidas-" + anho + "-" + mes+"-"+dia;
            JasperExportManager.exportReportToPdfFile(jasperprint, titulo + ".pdf");

            JasperViewer visor = new JasperViewer(jasperprint, false);
            visor.setTitle(titulo);
            visor.setVisible(true);
            visor.setFitWidthZoomRatio();

        } catch (JRException ex) {
            System.out.println("Error generando reporte " + ex);

        } catch (NullPointerException e) {
            System.err.println(e);

            JOptionPane.showMessageDialog(null, "No se encontraron resultados");
        } catch (SQLException ex) {
            System.err.println(ex);
        }

    }
}
