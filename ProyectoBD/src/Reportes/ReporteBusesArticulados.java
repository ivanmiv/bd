/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Reportes;

import java.io.InputStream;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Ivan
 */
public class ReporteBusesArticulados {

    public ReporteBusesArticulados() {

        ResultSet resultado = null;
        JOptionPane mensaje = new JOptionPane();
        int result = mensaje.showConfirmDialog(null, "Generando reporte",
        "", JOptionPane.OK_CANCEL_OPTION);
            
        if (result==2 || result == -1) return;
        try {

            resultado = new DAO.controladores.ControladorBus().consultarBusesArticuladosConductores();

            InputStream template = getClass().getResourceAsStream("/Reportes/ReporteBusesArticulados.jasper");

            JasperReport reporte = (JasperReport) JRLoader.loadObject(template);

            Map param = new HashMap();

            JasperPrint jasperprint = JasperFillManager.fillReport(reporte, param, new JRResultSetDataSource(resultado));

            JasperExportManager.exportReportToPdfFile(jasperprint, "Articulados" + ".pdf");

            JasperViewer visor = new JasperViewer(jasperprint, false);
            visor.setTitle("Articulados");

            visor.setVisible(true);
            visor.setFitWidthZoomRatio();

        } catch (JRException ex) {
            System.out.println("Error generando reporte " + ex);           

        } catch (NullPointerException e) {
            System.err.println(e);          

            JOptionPane.showMessageDialog(null, "No se encontraron resultados");
        }

    }
}
